<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\QueryFromContactForm;
use App\Models\CustomerQuery;
use Illuminate\Support\Facades\Input;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contact.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function store(ContactRequest $request)
    {
        try {
            Mail::to($request->email)->send(new QueryFromContactForm);
            $query = new CustomerQuery;

                $query->name = $request->name;
                $query->email = $request->email;
                $query->type = $request->type;
                $query->message = $request->message;
                $query->phone = $request->phone;
                $query->save();

                Mail::send([], [], function ($message) use ($request) {
                    $message->to('toi.outsmartcancer@gmail.com')
                      ->subject('Contact Form Query')
                      ->setBody('Query From Contact Form
                      Name: '.$request->name.'
                      Email: ' . $request->email.'
                      Type: ' .$request->type.'
                      Message:'.$request->message.'
                      Phone:'.$request->phone);
                  });

            return redirect('contact')->with('status', true)->withInput();
    } catch (\Exception $e) {

            return redirect('contact')->with('status', false)->withInput(Input::all());
    }

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //
    }
}
