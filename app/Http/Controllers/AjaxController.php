<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\City;
use GuzzleHttp\Client;

class AjaxController extends Controller
{
    public function cities($query, Request $request) {
        return City::where('name', 'like', $query.'%')->groupBy('name')->get();
    }

    public function states($query, Request $request) {
        return City::where('state', 'like', $query.'%')->groupBy('state')->get();
    }

    public function getPledgeCount() {
        // $client = new Client;
        // $response = $client->get('http://services.timesmobile.in/TOIPledgeAPI/GetCallCount.aspx?action=getcount');
        // return (json_decode($response->getBody()->getContents())->callcount) + \App\Models\User::where('has_pledged', 1)->count() + 350 + 398;
        return \App\Models\User::where('has_pledged', 1)->count() + 350 + 398 + 9701;
    }
}
