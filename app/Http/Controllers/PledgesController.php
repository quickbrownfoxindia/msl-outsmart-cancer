<?php

namespace App\Http\Controllers;

use App\Models\Pledge;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\PledgeRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\PledgeThankYou;

class PledgesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_old(PledgeRequest $request)
    {
        $authUser = \Auth::user();
        if( $authUser && $authUser->email == $request->email ) {
            $authUser->has_pledged = true;
            $authUser->city = $request->city;
            $authUser->phone = $request->phone;
            $authUser->state = $request->state;
            $authUser->save();
            Mail::to($request->email)->send(new PledgeThankYou);
            return redirect('/')->with('pledgeStatus', true);
        } else {
            \Auth::logout();
            $request->session()->flush();
            try {
                $authUser = User::where('email', $request->email)->firstOrFail();
                $authUser->name = $request->name;
                $authUser->email = $request->email;
                $authUser->state = $request->state;
                $authUser->city = $request->city;
                $authUser->phone = $request->phone;
                $authUser->password = \bcrypt(\str_random());
                $authUser->has_pledged = true;
                $authUser->save();
                Mail::to($request->email)->send(new PledgeThankYou);
                // \Auth::login($authUser, true);
                
            } catch(\Exception $e) {
                $authUser = new User;
                $authUser->name = $request->name;
                $authUser->email = $request->email;
                $authUser->state = $request->state;
                $authUser->city = $request->city;
                $authUser->phone = $request->phone;
                $authUser->password = \bcrypt(\str_random());
                $authUser->has_pledged = true;
                $authUser->save();
                Mail::to($request->email)->send(new PledgeThankYou);
                // \Auth::login($authUser, true);
            }
            return redirect('/')->with('pledgeStatus', true);
        }
        return redirect('/')->with('pledgeStatus', false);
    }


    public function store(PledgeRequest $request)
    {
        if(User::where('email', $request->email)->exists()) {
            $authUser = User::where('email', $request->email)->update([
                'name' => $request->name,
                'email' => $request->email,
                'state' => $request->state,
                'city' => $request->city,
                'phone' => $request->phone,
                'has_pledged' => true,
            ]);
            // Mail::to($request->email)->send(new PledgeThankYou);
            return redirect('/')->with('pledgeStatus', true)->with('pledgeMessage', 'You\'ve taken the pledge already!')
            ->with('pledgeType', 'old');
        } else {
            $authUser = new User;
            $newPassword = \str_random();
            $authUser->name = $request->name;
            $authUser->email = $request->email;
            $authUser->state = $request->state;
            $authUser->city = $request->city;
            $authUser->phone = $request->phone;
            $authUser->password = \bcrypt($newPassword);
            $authUser->has_pledged = true;
            $authUser->save();
            Mail::to($request->email)->send(new PledgeThankYou($authUser, $newPassword));
            return redirect('/')->with('pledgeStatus', true)->with('pledgeMessage', 'Thank you for taking a pledge towards better health and a happier tomorrow.</br>
            Do share, and help spread the word about early cancer screening. Read more <a href="/all-about-cancer">here</a>.')
            ->with('pledgeType', 'new');
        }
        return redirect('/')->with('pledgeStatus', false);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pledge  $pledge
     * @return \Illuminate\Http\Response
     */
    public function show(Pledge $pledge)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pledge  $pledge
     * @return \Illuminate\Http\Response
     */
    public function edit(Pledge $pledge)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pledge  $pledge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pledge $pledge)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pledge  $pledge
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pledge $pledge)
    {
        //
    }
}
