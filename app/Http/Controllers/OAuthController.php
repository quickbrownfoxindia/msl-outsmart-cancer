<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\Models\User;
use App\Models\Provider;

class OAuthController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider) {

        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider) {
        $user = Socialite::driver($provider)->user();

        $provider = Provider::where('name', $provider)->first();
        $authUser = $this->findOrCreateUser($user, $provider);
        \Auth::login($authUser, true);
        // session(['myshopify_domain' => $user->user['myshopify_domain']]);
        return redirect('/');
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        // dd($user);
        try {
            $authUser = User::where('email', $user->email)->firstOrFail();
            
        } catch(\Exception $e) {
            $authUser = User::create([
                'name'     => $user->name,
                'email'    => $user->email,
                'password' => \bcrypt(\str_random())
            ]);
            
        }
        
        $providerExists = \DB::table('provider_user')
            ->where('meta_provider_id', $user->id)
            ->where('user_id', $authUser->id)
            ->where('provider_id', $provider->id)
            ->count() > 0;

        
        if(!$providerExists) {
            $authUser->provider()->save($provider, ['meta_provider_id' => $user->id, 'token' => $user->token]);
        }
        return $authUser;
    }

}
