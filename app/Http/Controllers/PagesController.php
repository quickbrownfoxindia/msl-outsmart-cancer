<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function allAboutCancer(Request $request) {
        return view('pages.all-about-cancer');
    }

    public function faqs(Request $request) {
        return view('pages.faqs');
    }

    public function cancerScreening(Request $request) {
        return view('pages.cancer-screening');
    }

    public function getInvolved(Request $request) {
        return view('pages.get-involved');
    }
    
    public function partners(Request $request) {
        return view('pages.partners');
    }
}
