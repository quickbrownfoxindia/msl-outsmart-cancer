<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'name' => 'required',
            'message' => '',
            'phone' => 'integer|digits:10',
            'type' => '',
        ];
    }
    
    public function messages()
    {
        return [
            'email.email' => 'Please enter a valid email address',
            'email.required' => 'Email address is required',
            'message.required' => 'Message is required',
            'name.required' => 'Name is required',
            'type.required' => 'Query Type is required',
            'phone.required' => 'Phone Number is required',
            'phone.integer' => 'Phone enter a valid 10 digit mobile number',
            'phone.digits' => 'Phone enter a valid 10 digit mobile number',
        ];
    }
}
