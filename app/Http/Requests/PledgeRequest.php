<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PledgeRequest extends FormRequest
{
    protected $errorBag = 'pledge';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'name' => 'required',
            'state' => 'required',
            'city' => 'required',
            'phone' => 'required|integer|digits:10',
        ];
    }
    
    public function messages()
    {
        return [
            'email.email' => 'Please enter a valid email address',
            'email.required' => 'Email Address is required',
            'name.required' => 'Name is required',
            'state.required' => 'State is required',
            'city.required' => 'City is required',
            'phone.required' => 'Phone is required',
            'phone.integer' => 'Phone enter a valid 10 digit mobile number',
            'phone.digits' => 'Phone enter a valid 10 digit mobile number',
        ];
    }
}
