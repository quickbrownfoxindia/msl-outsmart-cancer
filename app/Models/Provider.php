<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    function user() {
        return $this->belongstoMany(\App\Models\User::class);
    }
}
