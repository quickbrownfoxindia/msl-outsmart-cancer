<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use DevDojo\Chatter\Models\Models;
use Mail;
use App\Mail\NewDiscussionPosted;

class ChatterAfterNewDiscussion
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event) {
        $post = ($event->post);
        $discussion = ($event->discussion);
        Mail::to(['hello@neerajkumar.name','pavankumar_bs@apollohospitals.com','johnchandy_t@apollohospitals.com','vishal.arora.msl@gmail.com'])->send(new NewDiscussionPosted($discussion, $post));
    }
}
