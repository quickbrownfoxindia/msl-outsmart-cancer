<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\NewResponsePosted;
use Mail;

class ChatterAfterNewResponse
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $post = ($event->post);
        Mail::to($post->discussion()->first()->user()->first())->send(new NewResponsePosted($post));
    }
}
