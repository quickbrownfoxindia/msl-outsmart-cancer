<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MissedCallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'register:missedcall';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends a get request to TOI\'s missed call API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

    }
}
