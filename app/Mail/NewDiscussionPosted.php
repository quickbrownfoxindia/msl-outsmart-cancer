<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use DevDojo\Chatter\Models\Discussion;
use DevDojo\Chatter\Models\Post;

class NewDiscussionPosted extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Discussion $discussion, Post $post)
    {
        $this->discussion = $discussion;
        $this->post = $post;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.ask_an_expert.new_discussion')->with('discussion', $this->discussion)->with('post', $this->post);
    }
}
