$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('[data-trigger=slick]').slick({
        slidesToShow: 1,
        dots: true,
        centerMode: false,
        dots: true,
        infinite: false,
        adaptiveHeight: false,
        autoplay: true,
        autoplaySpeed: 8000,
        cssEase: 'linear',
    }).on('afterChange', function(event, slick, currentSlide) {
            // console.log(currentSlide);
            if(currentSlide == 3) {
                $(slick).slickPause();
            }
    });

    window.odometerOptions = {
        auto: false, // Don't automatically initialize everything with class 'odometer'
        selector: '.my-numbers', // Change the selector used to automatically find things to be animated
        format: '(,ddd).dd', // Change how digit groups are formatted, and how many digits are shown after the decimal point
        duration: 3000, // Change how long the javascript expects the CSS animation to take
        theme: 'plaza', // Specify the theme (if you have more than one theme css file on the page)
        animation: 'count' // Count is a simpler animation method which just increments the value,
        // use it when you're looking for something more subtle.
    };
    var el = document.querySelector('#counter');

    window.od = new Odometer({
        el: el,
        value: 0,

        // Any option (other than auto and selector) can be passed in here
        format: '',
        theme: 'plaza'
    });

    // od.update(555)
    // or
    // el.innerHTML = 555

    $(".global-social-share").jsSocials({
        showLabel: false,
        showCount: false,
        shareIn: "popup",
        shares: ["facebook", "twitter", "pinterest", 'linkedin', "googleplus"]
    });


    $('select#state[type=text]').selectize({
        valueField: 'state',
        labelField: 'state',
        searchField: 'state',
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: '/ajax/states/' + encodeURIComponent(query),
                type: 'GET',
                error: function() {
                    callback();
                },
                success: function(res) {
                    console.log(res);
                    callback(res);
                    // res.repositories.slice(0, 10);
                    // console.log(res);
                    // return res;
                }
            });
        }
    });

    $('select#city[type=text]').selectize({
        valueField: 'name',
        labelField: 'name',
        searchField: 'name',
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: '/ajax/cities/' + encodeURIComponent(query),
                type: 'GET',
                error: function() {
                    callback();
                },
                success: function(res) {
                    console.log(res);
                    callback(res);
                    // res.repositories.slice(0, 10);
                    // console.log(res);
                    // return res;
                }
            });
        }
    });

    $('[data-trigger=ga]').on('click', function() {
        console.log('heh');
        gtag('event', $(this).data('action'), {
            'event_category': $(this).data('category'),
            'event_label': $(this).data('label')
          });
    });

    $(".contact #type").on('change', function() {
        if($(this).val() == 'cancer') {
            $('html, body').animate({ scrollTop: $("#partners").offset().top }, 200); 
        }
    })

    $('[data-toggle="tooltip"]').tooltip()


    $('.infographic .hotspot').each(function(){
	
        var $this = $(this),
                top = $this.data('top'),
                left = $this.data('left');
        
        $this.css({
            top: top + "%",
            left: left + "%"
        })
        .addClass('is-visible');

        // $(this).find('.icon').css('background-image', 'url(' + ($(this).find('.icon').attr('data-original-img')) + ')');
        
    });
    
    $(".hotspot-details").on('click', function(e){
        $(this).removeClass('is-visible');
        $(this).parents('.infographic').find('.hotspot').removeClass('is-active');
        e.preventDefault();
    });
    
    $('.hotspot').on('click', function(e){
        
        var el = $($(this).attr('data-target'));
        // console.log(el[0]);
        
        if(!$(this).hasClass('is-active'))
        {
            $(this).parents('.infographic').find('.hotspot').removeClass('is-active');
            $(this).addClass('is-active');
            // $(this).find('.icon').css('background-image', 'url(' + $(this).find('.icon').attr('data-active-img') + ')');

            /* details */
            $(this).parents('.infographic').find('.hotspot-details .details').removeClass('is-visible')
            $(this).parents('.infographic').find('.hotspot-details').find(el).addClass('is-visible');
        }
        else
        {
            $(this).removeClass('is-active');
            // $(this).find('.icon').css('background-image', 'url(' + ($(this).find('.icon').attr('data-original-img')) + ')');
            /* details */
            $(this).parents('.infographic').find('.hotspot-details .details').removeClass('is-visible')
            $(this).parents('.infographic').find('.hotspot-details').find(el).removeClass('is-visible');	
        }
        
        e.preventDefault();
    });


    $('.nav.nav-pills.nav-justified').on('shown.bs.tab', function (e) {
        $(e.target).parent().addClass('active') // newly activated tab
        $(e.relatedTarget).parent().removeClass('active') // previous active tab
    })
    

});

$(document).ready(function () {
    // Custom 
    var stickyToggle = function (sticky, stickyWrapper, scrollElement) {
        var stickyHeight = sticky.outerHeight();
        var stickyTop = stickyWrapper.offset().top;
        if (scrollElement.scrollTop() >= stickyTop) {
            stickyWrapper.height(stickyHeight);
            sticky.addClass("is-sticky");
        }
        else {
            sticky.removeClass("is-sticky");
            stickyWrapper.height('auto');
        }
    };

    // Find all data-toggle="sticky-onscroll" elements
    $('[data-toggle="sticky-onscroll"]').each(function () {
        var sticky = $(this);
        var stickyWrapper = $('<div>').addClass('sticky-wrapper'); // insert hidden element to maintain actual top offset on page
        sticky.before(stickyWrapper);
        sticky.addClass('sticky');

        // Scroll & resize events
        $(window).on('scroll.sticky-onscroll resize.sticky-onscroll', function () {
            stickyToggle(sticky, stickyWrapper, $(this));
        });

        // On page load
        stickyToggle(sticky, stickyWrapper, $(window));
    });
});



$(document).ready(function () {

    var form = $('#contact-form');

    form.submit(function (e) {

        e.preventDefault();
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize(),
            dataType: 'json',
            success: function (json) {
                // Success
                // Do something like redirect them to the dashboard...
            },
            error: function (json) {
                if (json.status === 422) {
                    var errors = json.responseJSON;
                    $.each(json.responseJSON, function (key, value) {
                        $('.' + key + '-error').html(value);
                    });
                } else {
                    // Error
                    // Incorrect credentials
                    // alert('Incorrect credentials. Please try again.')
                }
            }
        });
    });

});