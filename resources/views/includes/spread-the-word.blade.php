<div class="section pt-3 pb-3">
    <div class="container">
        <div class="row h-100">
            <div class="col-md-8 col-xs-6 my-auto">
                <h4 class="text-primary text-uppercase">Spread The Word</h4>
                <p class="m-0 text-gray lead">Take the first step today to let your friends and family know.</p>
            </div>
            <div class="col-md-4 col-xs-6 text-md-right text-lg-right my-auto">
                <div class="global-social-share"></div>
            </div>
        </div>
    </div>
</div>