<div class="section pt-5 pb-5 {{ $extraClass }} our-partners">
    <div class="container text-center">
        <div class="row">

            <div class="col-12 mb-5">
                <h2>OUR PARTNERS</h2>
            </div>
            <div class="col-6" style="border-right: 1px dotted #ffffff">
                <p>NGO PARTNERS</p>
                <div class="row pb-5">
                    <div class="col-sm-12 col-xs-12 col-md-4">
                        <a href="http://www.indiancancersociety.org/" target="_blank"><img class="img-fluid" width="150" height="150" src=" {{  asset('images/ics.jpg') }}" /></a>
                    </div>
                    <div class="col-sm-12 col-xs-12 col-md-4">
                        <a href="https://www.facebook.com/LifeAgainIndia/" target="_blank"><img class="img-fluid" width="150" height="150" src=" {{  asset('images/life-again.jpg') }}" /></a>
                    </div>
                    <div class="col-sm-12 col-xs-12 col-md-4">
                        <a href="http://www.cansupport.org/" target="_blank"><img class="img-fluid" width="150" height="150" src=" {{  asset('images/can-support.jpg') }}" /></a>
                    </div>
                </div>
            </div>
            <div class="col-6 text-center">
                <p>HAIR DONATION PARTNERS</p>
                <div class="row pb-5">
                    <div class="col-sm-12 col-xs-12 col-md-4">

                    </div>
                    <div class="col-sm-12 col-xs-12 col-md-4">

                        <a href="http://www.naturals.in/our-salons/" target="_blank"><img class="img-fluid" width="150" height="150" src=" {{  asset('images/naturals.jpg') }}" /></a>
                    </div>
                    <div class="col-sm-12 col-xs-12 col-md-4">

                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 pt-5 pb-5">
            <a href="{{ url('partners') }}" class="btn btn-light pl-4 pr-4">KNOW MORE</a>
        </div>
    </div>
</div>