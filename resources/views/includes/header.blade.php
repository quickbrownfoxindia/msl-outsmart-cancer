<div class="header pt-2 pb-2">
    <div class="container">
        <div class="row h-100">
            <div class="col-md-6 col-xs-12 pb-3">
                <div class="row">
                    <div class="col-4">
                        <a href="{{ url('/') }}"><img class="img-fluid logo mt-2" height="70" src="{{ asset('images/outsmart-cancer.png') }}" style="border-right: 2px dotted #999; margin-right: .5em; padding-right: .5em"
                        /></a>
                    </div>
                    <div class="col-4 text-center">
                        <span class="small">PRESENTED BY</span>
                        <a target="_blank" href="https://www.apollohospitals.com/"><img class="img-fluid border-0" height="60" src="{{ asset('images/apollo.png') }}" /></a>
                    </div>
                    <div class="col-4 text-center">
                        <span class="small">AN INITIATIVE BY</span>
                        <a target="_blank" href="http://timesofindia.indiatimes.com/"><img class="img-fluid border-0" height="60" src="{{ asset('images/toi.png') }}" /></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-12"></div>
            <div class="col-md-3 col-xs-12 text-center pb-3 my-auto">
                <div class="text-center">
                    <h6 class="text-primary text-uppercase">
                        <strong>Pledges Taken</strong>
                    </h6>
                <div id="counter"></div>
                </div>
            </div>
        </div>
    </div>
</div>