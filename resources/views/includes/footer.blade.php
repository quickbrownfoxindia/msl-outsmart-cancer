<div class="footer small pt-3 pb-0 bg-primary text-white">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 mb-2 text-center text-sm-left text-md-left text-ld-left">
                <p>Copyright &copy; 2018. All rights reserved. The Times of India.</p>
            </div>
            <div class="col-md-6 col-sm-6 mb-2 text-md-right text-lg-right text-center text-sm-center">
                <p><a target="_blank" href="https://www.facebook.com/TOIMYTIMES/"><i class="text-primary bg-white fa fa-facebook"></i></a> <a target="_blank" href="https://twitter.com/TOI_MyTimes"><i class="text-primary bg-white fa fa-twitter"></i></a></p>
                <p><i class="fa fa-envelope-o"></i> <a href="mailto:info@outsmartcancer.in">info@outsmartcancer.in</a> &middot; <i class="fa fa-phone"></i> <a href="tel:8448198134" class="text-light">8448198134</a></p>
            </div>
    </div>
</div>