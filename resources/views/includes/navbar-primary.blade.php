<div class="navigation bg-primary text-uppercase" data-toggle="sticky-onscroll">
    <div class="container">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand d-block d-sm-none" href="#">Outsmart Cancer</a>    
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                    <!-- <li class="nav-item {{ Request::segment(1) == '' ? 'active' : null }}">
                        <a class="nav-link" href="{{ url('/') }}">Home
                            <span class="sr-only">(current)</span>
                        </a>
                    </li> -->
                    <li class="nav-item {{ Request::segment(1) === 'all-about-cancer' ? 'active' : null }}">
                        <a class="nav-link" href="{{ url('all-about-cancer') }}">All About Cancer</a>
                    </li>
                    <li class="nav-item {{ Request::segment(1) === 'cancer-screening' ? 'active' : null }}">
                        <a class="nav-link" href="{{ url('cancer-screening') }}">Cancer Screening</a>
                    </li>
                    <li class="nav-item {{ Request::segment(1) === 'faqs' ? 'active' : null }}">
                        <a class="nav-link" href="{{ url('faqs') }}">FAQs</a>
                    </li>
                    <li class="nav-item {{ Request::segment(1) === 'get-involved' ? 'active' : null }}">
                        <a class="nav-link" href="{{ url('get-involved') }}">Get Involved</a>
                    </li>
                    <li class="nav-item {{ Request::segment(1) === 'partners' ? 'active' : null }}">
                        <a class="nav-link" href="{{ url('partners') }}">Partners</a>
                    </li>
                    <li class="nav-item {{ Request::segment(1) === 'contact' ? 'active' : null }}">
                        <a class="nav-link" href="{{ url('contact') }}">Contact Us</a>
                    </li>
                </ul>
                <ul class="navbar-nav mr-0">
                    <li class="nav-item">
                        <a class="btn btn-light btn-pledge text-uppercase mr-1 " href="{{ url('/#ask-an-expert-wrapper') }}">Ask an Expert</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-light btn-pledge text-uppercase mr-1 " href="{{ url('/#pledge-form-wrapper') }}">Take a Pledge</a>
                    </li>
                </ul>
                 <ul class="navbar-nav mr-0">
                    @auth
                    <li class="nav-item">
                        <a class="nav-link text-light" href="{{ url('logout') }}"><i class="fa fa-lg fa-sign-out"></i></a>
                    </li>
                    @else
                @endauth
                </ul>
        
        </div>
    </nav>
</div>
</div>