<div class="ask-an-expert">
<?php if($type == 'two') { ?>
<div class="section bg-light pt-4 pb-4">
    <div class="container">
        <div class="row h-100">

            <div class="col-md-6 col-xs-12 my-auto">
                <p class="lead m-0 mb-3"><strong>We are there for all the help you need. Ask us a question.</strong></p>
            </div>
            <div class="col-md-6 col-xs-12 my-auto text-md-right text-lg-right">
            <a href="{{ url('ask-an-expert') }}" class="btn btn-primary pr-5 pl-5 mr-3 mb-3" data-toggle="tooltip" data-placement="top" title="Coming soon">ASK AN EXPERT</a><a href="{{ url('contact') }}" onclick="" class="btn btn-gray pr-5 pl-5 mb-3">CONTACT US</a>
            </div>
        </div>
    </div>
</div>
<?php } else { ?>
<div class="section">
    <div class="container">
        <div class="row h-100 pt-5 pb-5">

            <div class="col-md-8 col-xs-12 my-auto">
                <p class="lead text-primary text-uppercase m-0"><strong>We are there for all the help you need. Ask us a question.</strong></p>
            </div>
            <div class="col-md-4 col-xs-12 my-auto text-md-right text-lg-right">
            <a href="{{ url('ask-an-expert') }}" class="btn btn-primary pr-5 pl-5" data-toggle="tooltip" data-placement="top" title="Coming soon">ASK AN EXPERT</a>
            </div>
        </div>
    </div>
</div>
<?php } ?>
</div>