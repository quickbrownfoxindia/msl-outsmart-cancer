<div class="section pt-3 pb-3" id="ask-an-expert-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="text-primary">ASK AN EXPERT</h1>
                <p class="lead m-0">Ask your question to Apollo Cancer Centre Specialists.</p>
            </div>
        </div>
    </div>
</div>
<hr class="m-0 p-0" />
<div class="section pb-5 pt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-12 pr-5 pl-5 pb-5 tabs">
                <div class="nav flex-column nav-pills" id="v-pills-tab1" role="tablist" aria-orientation="vertical">
                    <a class="nav-link  border" id="latest-queries-tab" data-toggle="pill" href="#latest-queries" role="tab" aria-controls="latest-queries"
                        aria-selected="true">Queries Received
                        <i class="fa fa-angle-right pull-right"></i>
                    </a>
                    <a class="nav-link border active border-top-0" id="ask-question-tab" data-toggle="pill" href="#ask-question" role="tab" aria-controls="ask-question-tab"
                        aria-selected="false">My Questions
                        <i class="fa fa-angle-right pull-right"></i>
                    </a>

                </div>
            </div>
            <div class="col-md-6 col-xs-12 ask-expert-queries-list">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade" id="latest-queries" role="tabpanel" aria-labelledby="latest-queries">
                        <div id="accordion4" role="tablist">


                        @auth
                        <?php if(!\DevDojo\Chatter\Models\Models::discussion()->has('posts', '>', 1)->orderBy('created_at', 'DESC')->count()) { ?>
                            <p>We have not received any query yet.</p>
                        <?php } else { ?>
                            <?php $ctr = 0; foreach(\DevDojo\Chatter\Models\Models::discussion()->has('posts', '>', 1)->with('user')->orderBy('created_at', 'DESC')->limit(10)->get() as $discussion) { $ctr++;?>
                                <div class="card">
                                    <div class="card-header collapsed" role="tab" id="heading<?php echo $discussion->id ?>" data-toggle="collapse" href="#collapse<?php echo $discussion->id ?><?php echo $discussion->post->first()->id ?>" aria-expanded="true" aria-controls="collapse<?php echo $discussion->id ?>">
                                            <h5 class="mb-0 d-inline text-primary">{{ $discussion->title }}</h5>
                                    </div>
        
                                        <div id="collapse<?php echo $discussion->id ?><?php echo $discussion->post->first()->id ?>" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion4">
                                            <div class="card-body">
                                            <?php echo $discussion->post()->skip(1)->take(1)->get()->first()->body; ?>
                                            </div>
                                        </div>
                                    </div>
                            <?php } ?>
                        <?php } ?>
                        @else
                            <p>We have not received any query yet.</p>
                        @endauth

                        </div>
                    </div>
                    <div class="tab-pane fade  show active" id="ask-question" role="tabpanel" aria-labelledby="ask-question">

                        @auth

                        <form class="form" action="{{ url('ask-an-expert/question') }}" method="post">
                            <div class="form-group">
                                <input class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title" placeholder="Enter title here"
                                />
                                <span class="text-danger">{{ $errors->first('title') }}</span>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control {{ $errors->has('body') ? 'is-invalid' : '' }}" name="body" placeholder="Type your question here"></textarea>
                                <span class="text-danger">{{ $errors->first('body_content') }}</span>
                            </div>
                            <input type="hidden" name="chatter_category_id" value="1" />
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary pull-right">SUBMIT</button>
                            </div>
                        </form>

                        <div class="clearfix"></div>

                        <div class="row mt-3">
                            <div class="col-md-12">
                                    <div id="my-queries" role="tablist">
                                            <?php $ctr = 0; foreach(\DevDojo\Chatter\Models\Models::discussion()->with('user')->where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->limit(5)->get() as $discussion) { $ctr++;; ?>
                                                <div class="card">
                                                        <div class="card-header collapsed" role="tab" id="heading-my-queries-<?php echo $discussion->id ?>" data-toggle="collapse" href="#collapse-my-queries-<?php echo $discussion->id ?><?php echo $ctr ?>" aria-expanded="true" aria-controls="collapse-my-queries-<?php echo $discussion->id ?>">
                                                            <h5 class="mb-0 d-inline text-primary">
                        
                                                            {{ $discussion->title }}
                                                                <br/>
                                                                <small class="text-gray"><?php echo ($discussion->post()->count() > 1)? 'Replied' : 'Not Replied'; ?></small>
                        
                                                            </h5>
                                                        </div>
                        
                                                        <div id="collapse-my-queries-<?php echo $discussion->id ?><?php echo $ctr ?>" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#my-queries">
                                                            <div class="card-body">

                                                            <?php if($discussion->post()->count() > 1) { ?>
                                                                <?php echo $discussion->post()->skip(1)->take(1)->get()->first()->body; ?>
                                                            <?php } else { ?>
                                                                <p>We have not received any reply</p>
                                                            <?php } ?>
                                                            
                                                                <br/>
                                                                <!-- <a class="btn btn-primary" href="{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.post') }}/{{ $discussion->id }}">View</a> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                            <?php } ?>
                                            
                                        </div>
                            </div>
                        </div>
                        


                        @else

                        <div class="form-group">
                            <div class="col-md-12">
                                <h4 class="text-primary">Please register to post a question</h4>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <p><a class="pr-2 pl-2 pt-1 pb-1" href="{{ url('oauth/facebook') }}"><img style="width: 150px" src="{{ asset('images/login/facebook.png') }}" /></a> 
                                or 
                                <a class="pr-2 pl-2 pt-1 pb-1" href="{{ url('oauth/google') }}"><img style="width: 150px" src="{{ asset('images/login/google.png') }}" /></a>
                                </p>
                                <strong>or</strong>
                            </div>
                        </div>
                                                
                        <form class="form-horizontal" method="POST" action="/register">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-12 control-label">E-Mail Address</label>

                                <div class="col-md-12">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required> 
                                    
                                    @if ($errors->has('email'))
                                    <span class="help-block small text-danger">
                                        {{ $errors->first('email') }}
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-12 control-label">Password</label>

                                <div class="col-md-12">
                                    <input id="password" type="password" class="form-control" name="password" required> @if ($errors->has('password'))
                                    <span class="help-block small text-danger">
                                        {{ $errors->first('password') }}
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>

                                    <a class="btn btn-link" href="{{ route('login') }}">
                                        Already Registered? Login Instead.
                                    </a>
                                </div>
                            </div>
                        </form>

                        @endauth
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>