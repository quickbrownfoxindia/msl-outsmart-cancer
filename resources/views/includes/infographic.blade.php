<div class="infographic pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center mb-3">
                <h4 class="text-primary text-uppercase">Various types of Cancer basis organs</h4>
                
            </div>
            <div class="col-6">
                <div class="image" style="">
                    <div class="contain p-5">
                        <a class="hotspot is-active" data-left="41" data-top="3" data-target=".brain">
                            <div class="point">
                                <img width="10" src="{{ asset('images/infographic/icons/point.png') }}" />
                            </div>
                            <div class="line" style="width: 70px"></div>
                            <div class="icon purple" style="background-image: url('{{ asset('images/infographic/icons/Brain_P.png') }}')"></div>
                            <div class="icon gray"  style="background-image: url('{{ asset('images/infographic/icons/Brain_G.png') }}')"></div>
                        </a>
                        <a class="hotspot" data-left="41" data-top="14" data-target=".thyroid">
                            <div class="point">
                                <img width="10" src="{{ asset('images/infographic/icons/point.png') }}" />
                            </div>
                            <div class="line" style="width: 70px"></div>
                            <div class="icon purple" style="background-image: url('{{ asset('images/infographic/icons/Thyroid_P.png') }}')"></div>
                            <div class="icon gray"  style="background-image: url('{{ asset('images/infographic/icons/Thyroid_G.png') }}')"></div>
                        </a>
                        <a class="hotspot" data-left="42" data-top="25" data-target=".lungs">
                            <div class="point">
                                <img width="10" src="{{ asset('images/infographic/icons/point.png') }}" />
                            </div>
                            <div class="line" style="width: 65px"></div>
                            <div class="icon purple" style="background-image: url('{{ asset('images/infographic/icons/Lung_P.png') }}')"></div>
                            <div class="icon gray"  style="background-image: url('{{ asset('images/infographic/icons/Lung_G.png') }}')"></div>
                        </a>
                        <a class="hotspot" data-left="44" data-top="36" data-target=".colorectal">
                            <div class="point">
                                <img width="10" src="{{ asset('images/infographic/icons/point.png') }}" />
                            </div>
                            <div class="line" style="width: 58px"></div>
                            <div class="icon purple" style="background-image: url('{{ asset('images/infographic/icons/Colorectal_P.png') }}')"></div>
                            <div class="icon gray"  style="background-image: url('{{ asset('images/infographic/icons/Colorectal_G.png') }}')"></div>
                        </a>
                        <a class="hotspot" data-left="41" data-top="45" data-target=".prostate">
                            <div class="point">
                                <img width="10" src="{{ asset('images/infographic/icons/point.png') }}" />
                            </div>
                            <div class="line" style="width: 73px"></div>
                            <div class="icon purple" style="background-image: url('{{ asset('images/infographic/icons/Prostate_P.png') }}')"></div>
                            <div class="icon gray"  style="background-image: url('{{ asset('images/infographic/icons/Prostate_G.png') }}')"></div>
                        </a>
                        <a class="hotspot" data-left="20" data-top="65" data-target=".leukaemia">
                        <div class="icon purple" style="background-image: url('{{ asset('images/infographic/icons/Leukemia_P.png') }}')"></div>
                            <div class="icon gray"  style="background-image: url('{{ asset('images/infographic/icons/Leukemia_G.png') }}')"></div>
                            <div class="line" style="width: 40px"></div>
                            <div class="point">
                                <img width="10" src="{{ asset('images/infographic/icons/point.png') }}" />
                            </div>
                        </a>
                        <a class="hotspot" data-left="20" data-top="42" data-target=".gyn">
                            <div class="icon purple" style="background-image: url('{{ asset('images/infographic/icons/Gynaecological_P.png') }}')"></div>
                            <div class="icon gray"  style="background-image: url('{{ asset('images/infographic/icons/Gynaecological_G.png') }}')"></div>
                            <div class="line" style="width: 50px"></div>
                            <div class="point">
                                <img width="10" src="{{ asset('images/infographic/icons/point.png') }}" />
                            </div>
                        </a>
                        <a class="hotspot" data-left="20" data-top="32" data-target=".liver">
                            <div class="icon purple" style="background-image: url('{{ asset('images/infographic/icons/Liver_P.png') }}')"></div>
                            <div class="icon gray"  style="background-image: url('{{ asset('images/infographic/icons/Liver_G.png') }}')"></div>
                            <div class="line" style="width: 50px"></div>
                            <div class="point">
                                <img width="10" src="{{ asset('images/infographic/icons/point.png') }}" />
                            </div>
                        </a>
                        <a class="hotspot" data-left="20" data-top="23" data-target=".breasts">
                            <div class="icon purple" style="background-image: url('{{ asset('images/infographic/icons/Breast_P.png') }}')"></div>
                            <div class="icon gray"  style="background-image: url('{{ asset('images/infographic/icons/Breast_G.png') }}')"></div>
                            <div class="line" style="width: 50px"></div>
                            <div class="point">
                                <img width="10" src="{{ asset('images/infographic/icons/point.png') }}" />
                            </div>
                        </a>
                        <a class="hotspot" data-left="20" data-top="12" data-target=".head">
                            <div class="icon purple" style="background-image: url('{{ asset('images/infographic/icons/HeadNeck_P.png') }}')"></div>
                            <div class="icon gray"  style="background-image: url('{{ asset('images/infographic/icons/HeadNeck_G.png') }}')"></div>
                            <div class="line" style="width: 65px"></div>
                            <div class="point">
                                <img width="10" src="{{ asset('images/infographic/icons/point.png') }}" />
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class='col-6'>
                <div class="hotspot-details">

                    <div class="details thyroid">
                        <div class="bg-gray-lighter p-3">
                            <img class="icon" src="{{ asset('images/infographic/icons/Thyroid_P.png') }}" />
                            <h3 class="text-uppercase">Thyroid Cancer</h3>
                            <p> The thyroid is a butterfly shaped gland at the base of the throat, near the windpipe which produces
                                hormones that help control heart rate, blood pressure, body temperature and weight. </p>
                            <ul>
                                <li>There are four types of thyroid cancer - papillary, follicular, medullary and anaplastic
                                    thyroid cancer. </li>
                                <li>Papillary is the most common while anaplastic is the most aggressive and difficult to cure.
                                    Other types of thyroid cancers are usually curable if detected early.</li>
                                <li>Symptoms
                                    <ul>
                                        <li>Neck or facial pain</li>
                                        <li>Shortness of breath </li>
                                        <li>Difficulty in swallowing </li>
                                        <li>Hoarseness or voice change </li>
                                    </ul>
                                </li>
                            </ul>

                        </div>
                    </div>

                    <div class="details brain is-visible">
                        <div class="bg-gray-lighter p-3">
                            <img class="icon" src="{{ asset('images/infographic/icons/Brain_P.png') }}" />
                            <h3 class="text-uppercase">Brain Tumor</h3>
                            <p>Brain cancer or tumours is an abnormal growth of cells in the brain. </p>
                            <p>Malignant tumours can grow and spread aggressively to distant parts of the body as well.</p>
                            <p>Benign tumours are less harmful as compared to malignant ones as they don’t spread to nearby
                                tissues but a benign tumour can cause problems in the brain by pressing on a nearby tissue.
                            </p>
                            <p>Brain tumours that originate in brain cells are called primary brain tumours. Metastatic or secondary
                                brain tumours spread to the brain from other tumours. Symptoms of a brain tumour are usually
                                related to its location rather than its size and develops when the tumour destroys or compresses
                                normal brain tissue. </p>
                            <p>So either the tissues around the tumour swell or the tumour interferes with the normal flow of
                                fluid around the brain and the spinal cord.</p>
                            <p>Symtomps:</p>
                            <ul>
                                <li>Headache</li>
                                <li>Seizures</li>
                                <li>Speech problems</li>
                                <li>Imbalance or difficulty in walking</li>
                                <li>Impaired vision or a restricted visual field</li>
                            </ul>
                        </div>
                    </div>
                    <div class="details breasts">
                        <div class="bg-gray-lighter p-3">
                            <img class="icon" src="{{ asset('images/infographic/icons/Breast_P.png') }}" />
                            <h3 class="text-uppercase">Breast Cancer</h3>
                            <p>Breast Cancer is established as one of the nation’s most formidable enemies in the healthcare
                                landscape. It is one of the most common cancers amongst Indian women; every year as many
                                as 100,000 women develop breast cancer in the country. This high incidence is poised to rise
                                to an even more unmanageable number.</p>
                            <ul>
                                <li>Regular breast self examination is a must</li>
                                <li>
                                    Symptoms:
                                    <ul>
                                        <li>Swelling in the armpit</li>
                                        <li>A breast lump/thickening that feeds different </li>
                                        <li>Bloody discharge from nipple </li>
                                        <li>Change in the size, shape or appearance of the breast</li>
                                        <li>Changes in the breast skin, such as dimpling</li>
                                        <li>A newly inverted nipple </li>
                                        <li>Peeling, scaling or flaking of the area around the nipple or breast skin</li>
                                </li>
                                </ul>
                        </div>
                    </div>
                    <div class="details head">
                        <div class="bg-gray-lighter p-3">
                            <img class="icon" src="{{ asset('images/infographic/icons/HeadNeck_P.png') }}" />
                            <h3 class="text-uppercase">Head & Neck Cancer</h3>
                            <p>This makes up almost 30% of cancers among Indians. Causes are chewing tobacco, betel nut, paan,
                                smoking cigarettes and consuming excessive alcohol. Another risk factor is an infection with
                                the HPV which increases the risk of throat cancer</p>
                            <p>Symptoms:</p>
                            <ul>
                                <li> A sore in the mouth that has not healed for more than 3 weeks</li>
                                <li>Persistent change of voice </li>
                                <li>Difficulty in chewing and swallowing </li>
                                <li>A lump in the neck </li>
                                Bleeding, pain or numbness in the nose or mouth
                                <li>Difficulty in the opening the mouth</li>
                                <li>Facial, neck or ear pain</li>
                            </ul>

                        </div>
                    </div>


                    <div class="details lungs">
                        <div class="bg-gray-lighter p-3">
                            <img class="icon" src="{{ asset('images/infographic/icons/Lung_P.png') }}" />
                            <h3 class="text-uppercase">Lung Cancer</h3>
                            <p>Cancer in the lungs is common and people who smoke are at the highest risk of contracting lung
                                cancer. The risk of lung cancer increases with the duration of smoking period and the number
                                of cigarettes smoked. </p>
                            <ul>
                                <li>If one quits this habit even after several years, one can significantly reduce the chances
                                    of developing lung cancer.</li>
                                <li>Risk factors include smoking, passive smoking, exposure to radon gas, asbestos and a family
                                    history of lung cancer. People with an increased risk of lung cancer should take annual
                                    CT scans to look for the disease. </li>
                                <li>Also, smokers of 55 years of age or older and even those who used to smoke earlier should
                                    check with their doctor about screening for lung cancer.</li>
                                <li>Symptoms:
                                    <ul>

                                        <li>A new cough that doesn’t go away </li>
                                        <li>Changes in a chronic cough or ‘smoker cough’ </li>
                                        <li>Coughing blood, even small amounts</li>
                                        <li>Shortness of breath</li>
                                        <li>Chest pain</li>
                                        <li>Wheezing </li>
                                        <li>Hoarseness</li>
                                        <li>Unexpected weight loss</li>
                                        <li>Bone pain </li>
                                        <li>Headache </li>

                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="details prostate">
                        <div class="bg-gray-lighter p-3">
                            <img class="icon" src="{{ asset('images/infographic/icons/Prostate_P.png') }}" />
                            <h3 class="text-uppercase">Prostate Cancer</h3>
                            <p> This is one of the most common types of cancers in men. It usually affects men in their 60s,
                                but is now progressively found in men of a lower age group as well. The common problems arising
                                in this gland include benign (non-cancerous) enlargement, or cancer of the prostate. </p>
                            <p>The risk factors include older age, family history and obesity.</p>
                            <p>Prostate cancer grows slowly and initially remains confined to the organ. However, in certain
                                cases, the growth is rapid and can spread quickly to other organs. </p>
                            <p>Precaution: Early detection allows patients to choose from a range of treatment options, with
                                excellent outcomes.</p>
                            <p>Symptoms: </p>
                            <ul>
                                <li>Trouble urinating </li>
                                <li>Decreased force in the stream of urine</li>
                                <li>Blood in semen</li>
                                <li>Discomfort in the pelvic area</li>
                                <li>Erectile dysfunction</li>
                            </ul>

                        </div>
                    </div>

                    <div class="details gyn">
                        <div class="bg-gray-lighter p-3">
                            <img class="icon" src="{{ asset('images/infographic/icons/Gynaecological_P.png') }}" />
                            <h3 class="text-uppercase">Gynaecological Cancer</h3>
                            <p>The common gynaecological cancers are cervical, ovarian, endometrial (uterine body) and cancer
                                of the fallopian tube (occasionally) excluding carcinoma of the female breast. Gynaecological
                                cancers have increased in India and by 2020, may constitute about 30% of the total cancers
                                among women.</p>
                            <p>Symptoms: </p>
                            <ul>
                                <li>Irregular periods </li>
                                <li>Bleeding after sex</li>
                                <li>Bleeding after menopause </li>
                                <li>Persistent white/watery/foul discharge </li>
                            </ul>

                        </div>
                    </div>
                    <div class="details colorectal">
                        <div class="bg-gray-lighter p-3">
                            <img class="icon" src="{{ asset('images/infographic/icons/Colorectal_P.png') }}" />
                            <h3 class="text-uppercase">Colorectal Cancer</h3>
                            <p> This occurs when cells in the colon or rectum grow and multiply uncontrollably, damaging surrounding
                                tissue and interfering with their normal function. It is one of the slowest growing cancers,
                                with high chances of a complete cure, if detected early. The concern though lies in early
                                detection as major symptoms do not occur initially. </p>
                            <p>Major risk factors include family history, chronic inflammatory bowel diseases, age above 50,
                                obesity, physical inactivity, diet with less fibre, consumption of more red & processed meats,
                                smoking and alcoholism.</p>
                            <p>Symptoms:</p>
                            <ul>
                                <li>Blood in stool</li>
                                <li>Irregular bowel habits </li>
                                <li>Pain in abdomen </li>
                                <li>Anaemia due to blood loss</li>
                                <li>Weakness and fatigue</li>
                                <li>Decreased appetite or weight loss</li>
                            </ul>

                        </div>
                    </div>
                    <div class="details liver">
                        <div class="bg-gray-lighter p-3">
                            <img class="icon" src="{{ asset('images/infographic/icons/Liver_P.png') }}" />
                            <h3 class="text-uppercase">Liver Cancer</h3>
                            <p>The liver is one of the vital organs which filters the blood coming from the digestive tract,
                                before circulating it to the rest of the body. It can be affected either by primary liver
                                cancer, which arises in the liver, or secondary or metastatic cancer, which originates elsewhere
                                in the body.</p>
                            <p>Primary liver cancer (hepatocellular carcinoma) tends to occur when the liver is damaged in the
                                form of ‘Cirrhosis’ (a scarring condition of the liver), certain birth defects, alcohol abuse,
                                chronic infection with diseases such as hepatitis B and C, hemochromatosis, obesity and fatty
                                liver disease amongst others.</p>
                            <p>Symptoms: </p>
                            <ul>
                                <li>A hard lump on the right side just below the rib cage </li>
                                <li>Swollen abdomen and discomfort in the upper abdomen (right die)</li>
                                <li>Pain near the right shoulder blade or in the back</li>
                                <li>Jaundice </li>
                                <li>Easy bruising or bleeding</li>
                                <li>Unusual tiredness</li>
                                <li>Nausea and vomiting</li>
                                <li>Weight loss for no known reason</li>
                            </ul>

                        </div>
                    </div>
                    <div class="details leukaemia">
                        <div class="bg-gray-lighter p-3">
                            <img class="icon" src="{{ asset('images/infographic/icons/Leukemia_P.png') }}" />
                            <h3 class="text-uppercase">Leukemia Cancer</h3>
                            <p>Individuals who have had some types of radiation or chemotherapy for treating other cancers,
                                or those with certain genetic disorders, or exposure to chemicals like benzene or have family
                                history are often at risk for leukaemia. This Cancer develops when the genetic material,
                                or DNA, of a white blood cell is altered and the diseased cells called blasts eventually
                                take over the bone marrow and do not allow normal RBC, WBC and platelet development. </p>
                            <p>Symptoms</p>
                            <ul>
                                <li>Pallor </li>
                                <li>Lymph node swelling</li>
                                <li>Enlargement of liver </li>
                                <li>Spleen</li>
                                <li>Observing the complete blood count which would show abnormal levels of the blood cells</li>
                            </ul>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="bg-primary p-3 text-light">Disclaimer:  Having any of these symptoms does not mean it is cancer, but if one or more of them is noticed for more than two weeks, then a doctor must be seen and an immediate health screening is a must. </div>
            </div>
        </div>
    </div>
</div>