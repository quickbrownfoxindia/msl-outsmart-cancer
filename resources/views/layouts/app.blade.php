<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Pledge your support to #OutsmartCancer</title>

    <meta name="google-site-verification" content="rqW4o2qQCMiET34cKkwyNPUvZGXAsmTajAVKfj2lrfk" />
    <meta name="description" content="Take the first step towards getting your Cancer screening done.">
    <meta name="image" content="{{ asset('images/social.png') }}">
    <meta property="og:title" content="Pledge your support to #OutsmartCancer">
    <meta property="og:description" content="Cancer is a fatal disease. Fight it through early detection. Take a pledge & get your Cancer Screening done.">
    <meta property="og:image" content="{{ asset('images/social.png') }}">
    <meta name="twitter:card" content="summary_large_image">
    <meta property="og:site_name" content="Outsmart Cancer">
    <meta name="twitter:image:alt" content="Cancer is a fatal disease. Fight it through early detection. Take a pledge & get your Cancer Screening done.">
    <meta property="fb:app_id" content="{{ env('FB_API_KEY') }}" />
    <meta property="fb:admins" content="609558078" />
    <meta name="twitter:site" content="@TOI_MyTimes">

    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />


    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/application.css') }}" rel="stylesheet"> @yield('css')
    <style>

.selectize-input:after, .selectize-input::after {
    display: none !important;
}

a, button:focus {
    outline: none !important;
}


    </style>
</head>

<body class="{{ (collect(\Request::segments())->implode(' ') == '') ? 'root' : (collect(\Request::segments())->implode(' ')) }}">
    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: "<?php echo env('FB_API_KEY'); ?>",
                cookie: true,
                xfbml: true,
                version: 'v2.11'
            });

            FB.AppEvents.logPageView();

        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script> 
    
    <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','https://connect.facebook.net/en_US/fbevents.js');fbq('init', '1903399999937068'); fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" src="https://www.facebook.com/tr?id=1903399999937068&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-47168835-6"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-47168835-6');
    </script>
    
    @include('includes.header') @yield('content') @include('includes.footer')

    <!-- Scripts -->
    <script src="{{ mix('js/application.js') }}"></script> @yield('js')
    <script>
        $(document).ready(function () {
            $.get('/ajax/get-pledge-count', function(res) {
                window.od.update(res);
            })

            // window.od.update(12659);
        })
        <?php if(session('pledgeStatus') || $errors->pledge->any()) { ?>
            $('html, body').animate({ scrollTop: $('#pledge-form-wrapper').offset().top }, 'slow');
        <?php } ?>
    </script>
</body>

</html>