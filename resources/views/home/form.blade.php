@extends('layouts.app') @section('content')
<div class="hero home">


    <div class="item home-change-on-mobile" style="background-image: url('{{ asset('images/home-banner.png') }}'); background-position: top center;">
        <div class="overlay"></div>

        <div class="container h-100">
            <div class="row h-100">
                <div class="col-md-6 col-xs-12 my-auto">
                    <div class="caption">
                        <h1 class="text-uppercase title">The earliest signs of Cancer often come too late</h1>
                        <p>Pledge today and get your Cancer screening done.
                        </p>
                        <a href="#pledge-form-wrapper" class="btn btn-primary">
                            <img class="pull-left" height="20" src="{{ asset('images/pledge-icon.png') }}" />&nbsp;PLEDGE NOW</a>&nbsp;
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
@include('includes.navbar-primary')

<div id="pledge-form-wrapper" class="section pt-5 pb-5 bg-light" style="background: url('/images/home-bg-pattern.jpg'); background-position: center bottom; background-size: 100% 100%; background-repeat: no-repeat">
    <div class="container text-light">
        <div class="row" style="border-bottom: 5px solid #fff; background: url('images/pledge-icon.png'); background-repeat: no-repeat; background-position: 10px bottom; background-size: 75px">
            <div class="col-12" style="padding-left: 100px">
                <h1>TAKE THE PLEDGE</h1>
                <p class="lead">This is just the start. Take the first step towards getting your Cancer screening done.</p>
            </div>
        </div>
        @if (session('pledgeStatus') && session('pledgeStatus') == true)
        <div class="col-md-12 text-center mt-3">
            <div class="alert alert-light" role="alert">
                <p><?php echo session('pledgeMessage'); ?></p>
            </div>
        </div>

        @if(session('pledgeType') && session('pledgeType') == 'new')
            <!-- Google Code for Outsmart Cancer Pledge Conversion Page -->
            <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 877225107;
            var google_conversion_label = "BOqbCPy2gXsQk8mlogM";
            var google_remarketing_only = false;
            /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
            <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/877225107/?label=BOqbCPy2gXsQk8mlogM&amp;guid=ON&amp;script=0"/>
            </div>
            </noscript>
            <script>
                fbq('track', 'CompleteRegistration');
            </script>
            
        @endif

        @elseif(session('pledgeStatus') && session('pledgeStatus') == false)
        <div class="alert alert-danger mt-3">
            An error occured. Please try again later
        </div>
        @else


        @if ($errors->pledge->any())
            <div class="alert alert-danger mt-3">Please fill all form fields correclty.</div>
        @endif
        <div class="row mt-5">
            <div class="col-12">
                <form action="/pledges" method="post" id="pledge-form" class="w-100" autocomplete="off">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-row">
                        <div class="col-md-2 form-group offset-md-1">

                            <label class="text-uppercase d-none d-sm-block" for="name">Name *</label>
                            <input autocomplete="off" type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" id="name"
                                placeholder="Name">
                            <!-- <br/><span class="text-danger">{{ $errors->first('name') }}</span> -->
                        </div>
                        <div class="col-md-2 form-group ">

                            <label class="text-uppercase d-none d-sm-block" for="email">Email *</label>
                            <input autocomplete="off" type="email" class="form-control  {{ $errors->has('email') ? 'is-invalid' : '' }} " name="email"
                                id="email" placeholder="Email Address">
                            <!-- <br/><sp
                    </div>an class="text-danger">{{ $errors->first('email') }}</span> -->
                        </div>
                        <div class="col-md-2 form-group ">

                            <label class="text-uppercase d-none d-sm-block" for="phone">Phone Number *</label>
                            <input autocomplete="off" maxlength="10" type="text" class="form-control  {{ $errors->has('phone') ? 'is-invalid' : '' }}" name="phone"
                                id="phone" placeholder="Phone Number">
                            <!-- <br/><sp
                    </div>an class="text-danger">{{ $errors->first('phone') }}</span> -->
                        </div>
                        <div class="col-md-2 form-group ">

                            <label class="text-uppercase d-none d-sm-block" for="state">State *</label>
                            <select autocomplete="off" type="text" class="form-control  {{ $errors->has('state') ? 'is-invalid' : '' }}" name="state"
                                id="state" placeholder="State">
                                <option></option>
                            </select>
                            <!-- <br/><sp
                    </div>an class="text-danger">{{ $errors->first('state') }}</span> -->
                        </div>
                        <div class="col-md-2 form-group ">

                            <label class="text-uppercase d-none d-sm-block" for="city">City *</label>
                            <select autocomplete="off" type="text" class="form-control  {{ $errors->has('city') ? 'is-invalid' : '' }}" name="city" id="city"
                                placeholder="City">
                                <option></option>
                            </select>
                            <!-- <br/><span class="text-danger">{{ $errors->first('city') }}</span> -->
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-xs-12 col-md-10 offset-md-1 text-center text-md-right text-lg-right text-sm-right">
                            <button type="submit" class="btn btn-light">Pledge Now</button>
                        </div>
                        <div class="col-xs-12 col-md-10 text-center text-md-left text-lg-left text-sm-left ">
                            <a  data-trigger="ga" data-category="OCP" data-action="call" data-label="form_page" href="tel:8448198134" class="mt-3 text-white d-block d-sm-none">Or pledge via missed call at 8448198134</a>
                        </div>
                    </div>
                    <!-- <div class="form-row mt-3">
                        <div class="col-md-10 col-xs-6 offset-md-1 offset-xs-0 text-right">
                            <button type="submit" class="btn btn-light">Pledge Now</button>
                        </div>
                    </div>
                    <div class="form-row text-center mt-3">
                        <a href="tel:+911165416369" class="text-white d-block d-sm-none">Or Register Via Missed Call at +911165416369</a>
                    </div> -->
                </form>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection