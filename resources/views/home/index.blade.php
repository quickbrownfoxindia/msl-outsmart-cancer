@extends('layouts.app') @section('content')
<div class="hero home" data-trigger="slick">

    <div class="item home-change-on-mobile " style="background-image: url('{{ asset('images/home-banner-new.jpg') }}'); background-position: top center;">
        <div class="overlay"></div>

        <div class="container h-100">
            <div class="row h-100">
                <div class="col-md-6 col-xs-12 my-auto">
                    <div class="caption">
                        <h1 class="text-uppercase title">The earliest signs of Cancer often come too late</h1>
                        <p>Pledge today and get your Cancer screening done.
                            <!-- <a  href="tel:8448198134" class="text-light">8448198134</a> -->
                        </p>
                        <a href="#pledge-form-wrapper" class="btn btn-primary">
                            <img class="pull-left" height="20" src="{{ asset('images/pledge-icon.png') }}" />&nbsp;FILL THE FORM</a>&nbsp;<span class="text-light">OR</span>&nbsp;
                        <a data-trigger="ga" data-category="OCP" data-action="call" data-label="homepage" href="tel:8448198134" class="btn btn-light text-primary">
                            <i class="fa fa-phone"></i> Missed Call @ 8448198134</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="item" style="background-image: url('{{ asset('images/home-banner-hair.jpg') }}'); background-position: top center;">
        <div class="overlay"></div>

        <div class="container h-100">
            <div class="row h-100">
                <div class="col-md-6 col-xs-12 my-auto">
                    <div class="caption">
                        <h1 class="text-uppercase title">Donate 6 inches of your hair or get a purple streak to pledge your support for cancer survivors</h1>
                        <a href="#pledge-form-wrapper" class="btn btn-primary">
                            <img class="pull-left" height="20" src="{{ asset('images/pledge-icon.png') }}" />&nbsp;PLEDGE NOW</a>&nbsp;
                        <a href="{{ url('get-involved') }}" class="btn btn-light text-primary">
                            <i class="fa fa-star"></i> KNOW MORE</a>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="item" style="background-position: top center;">

        <iframe style="width: 100%" height="450" src="https://www.youtube.com/embed/D4KWHO5MtEs?controls=0" frameborder="0" gesture="media"
            allow="encrypted-media" allowfullscreen></iframe>

    </div>

    <div class="item" style="background-position: top center;">

        <iframe style="width: 100%" height="450" src="https://www.youtube.com/embed/eN1fWJksTFc?controls=0" frameborder="0" gesture="media"
            allow="encrypted-media" allowfullscreen></iframe>

    </div>



    
</div>
<!-- <div class="container">
    <div class="row">
        @auth
        <div class="col-md-6 offset-md-3 mt--5 bg-light p-3 text-center">
            <h2>Hello, {{ \Auth::user()->name }}!</h2>
        </div>
        @else
        <div class="col-md-6 offset-md-3 mt--5 bg-light p-3">
            <form>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="First name">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Last name">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Email Address">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Register</button>
                        </div>
                    </div>
                    <div class="col">
                        <a class="btn" href="{{ url('oauth/facebook') }}">Login using Facebook</a>
                    </div>

                </div>
            </form>
        </div>
        @endauth
    </div>
</div>
//-->
@include('includes.navbar-primary')

<div class="section pt-5 pb-5" style="background: #f4f4ff">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-primary" style="/* noticed the irony*/
                word-break: break-all;">
                    <strong>#OUTSMARTCANCER</strong>
                </h1>
                <p class="lead">You can Outsmart Cancer. </p>
                <p class="text-dark-gray light-weight">Cancer is devious. It hides in plain-sight, it disguises as minor discomforts and before you know it, it
                    consumes us. The only way to outsmart this disease operating in stealth is early detection through regular
                    screening. This way cancer can no longer hide inside of us. Pledge today to get your Cancer Screening
                    done.
                </p>
            </div>
        </div>
    </div>
</div>
<div id="pledge-form-wrapper" class="section pt-5 pb-5 bg-light" style="background: url('/images/home-bg-pattern.jpg'); background-position: center bottom; background-size: 100% 100%; background-repeat: no-repeat">
    <div class="container text-light">
        @auth
        <div class="col-md-12 text-center mt-3">
            <div class="alert alert-light" role="alert">
                <p>Thank you for taking a pledge towards better health and a happier tomorrow. Do share, and help spread the word about early cancer screening. Read more <a href="{{ url('ask-an-expert') }}">here</a>.</p>
            </div>
        </div>        
        @else
        <div class="row" style="border-bottom: 5px solid #fff; background: url('images/pledge-icon.png'); background-repeat: no-repeat; background-position: 10px bottom; background-size: 75px">        
            <div class="col-12" style="padding-left: 100px">
                <h1>TAKE THE PLEDGE</h1>
                <p class="lead">This is just the start. Take the first step towards getting your Cancer screening done.</p>
            </div>
        </div>
        @if (session('pledgeStatus') && session('pledgeStatus') == true)
        <div class="col-md-12 text-center mt-3">
            <div class="alert alert-light" role="alert">
                <p><?php echo session('pledgeMessage'); ?></p>
            </div>
        </div>        

        @if(session('pledgeType') && session('pledgeType') == 'new')
            <!-- Google Code for Outsmart Cancer Pledge Conversion Page -->
            <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 877225107;
            var google_conversion_label = "BOqbCPy2gXsQk8mlogM";
            var google_remarketing_only = false;
            /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
            <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/877225107/?label=BOqbCPy2gXsQk8mlogM&amp;guid=ON&amp;script=0"/>
            </div>
            </noscript>
            <script>
                fbq('track', 'CompleteRegistration');
            </script>
            
        @endif

        @elseif(session('pledgeStatus') && session('pledgeStatus') == false)
        <div class="alert alert-danger mt-3">
            An error occured. Please try again later
        </div>
        @else


        @if ($errors->pledge->any())
            <div class="alert alert-danger mt-3">Please fill all form fields correclty.</div>
        @endif
        <div class="row mt-5">
            <div class="col-12">
                <form action="/pledges" method="post" id="pledge-form" class="w-100" autocomplete="off">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-row">
                        <div class="col-md-2 form-group offset-md-1">

                            <label class="text-uppercase d-none d-sm-block" for="name">Name *</label>
                            <input autocomplete="off" type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" id="name"
                                placeholder="Name">
                            <!-- <br/><span class="text-danger">{{ $errors->first('name') }}</span> -->
                        </div>
                        <div class="col-md-2 form-group ">

                            <label class="text-uppercase d-none d-sm-block" for="email">Email *</label>
                            <input autocomplete="off" type="email" class="form-control  {{ $errors->has('email') ? 'is-invalid' : '' }} " name="email"
                                id="email" placeholder="Email Address">
                            <!-- <br/><sp
                    </div>an class="text-danger">{{ $errors->first('email') }}</span> -->
                        </div>
                        <div class="col-md-2 form-group ">

                            <label class="text-uppercase d-none d-sm-block" for="phone">Phone Number *</label>
                            <input autocomplete="off" maxlength="10" type="text" class="form-control  {{ $errors->has('phone') ? 'is-invalid' : '' }}" name="phone"
                                id="phone" placeholder="Phone Number">
                            <!-- <br/><sp
                    </div>an class="text-danger">{{ $errors->first('phone') }}</span> -->
                        </div>
                        <div class="col-md-2 form-group ">

                            <label class="text-uppercase d-none d-sm-block" for="state">State *</label>
                            <select autocomplete="off" type="text" class="form-control  {{ $errors->has('state') ? 'is-invalid' : '' }}" name="state"
                                id="state" placeholder="State">
                                <option></option>
                            </select>
                            <!-- <br/><sp
                    </div>an class="text-danger">{{ $errors->first('state') }}</span> -->
                        </div>
                        <div class="col-md-2 form-group ">

                            <label class="text-uppercase d-none d-sm-block" for="city">City *</label>
                            <select autocomplete="off" type="text" class="form-control  {{ $errors->has('city') ? 'is-invalid' : '' }}" name="city" id="city"
                                placeholder="City">
                                <option></option>
                            </select>
                            <!-- <br/><span class="text-danger">{{ $errors->first('city') }}</span> -->
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-xs-12 col-md-10 offset-md-1 text-center text-md-right text-lg-right text-sm-right">
                            <button type="submit" class="btn btn-light">Pledge Now</button>
                        </div>
                        <div class="col-xs-12 col-md-10 text-center text-md-left text-lg-left text-sm-left ">
                            <a data-trigger="ga" data-category="OCP" data-action="call" data-label="homepage" href="tel:8448198134" class="mt-3 text-white d-block d-sm-none">Or pledge via missed call at 8448198134</a>
                        </div>
                    </div>
                    <!-- <div class="form-row mt-3">
                        <div class="col-md-10 col-xs-6 offset-md-1 offset-xs-0 text-right">
                            <button type="submit" class="btn btn-light">Pledge Now</button>
                        </div>
                    </div>
                    <div class="form-row text-center mt-3">
                        <a href="tel:+911165416369" class="text-white d-block d-sm-none">Or Register Via Missed Call at +911165416369</a>
                    </div> -->
                </form>
            </div>
        </div>
        @endif
        @endauth
    </div>
</div>
@include('includes.spread-the-word')
<hr class="m-0" />
<div class="section pb-5 pt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-12 pr-5 pl-5 pb-5 tabs">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active border" id="v-pills-tab-1" data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-home"
                        aria-selected="true">All About Cancer
                        <i class="fa fa-angle-right pull-right"></i>
                    </a>
                    <a class="nav-link border border-top-0" id="v-pills-tab-2" data-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-profile"
                        aria-selected="false">Cancer Screening
                        <i class="fa fa-angle-right pull-right"></i>
                    </a>
                    <a class="nav-link border border-top-0" id="v-pills-tab-3" data-toggle="pill" href="#v-pills-3" role="tab" aria-controls="v-pills-messages"
                        aria-selected="false">Get Involved
                        <i class="fa fa-angle-right pull-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-8 col-xs-12">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-tab-1">
                        <h1 class="title">ALL ABOUT CANCER</h1>
                        <p class="tagline lead mb-1 text-light-gray">It’s time to get to know Cancer.</p>
                        <div class="short-left-border"></div>
                        <p class="description text-gray">
                            <img width="200" src="{{ asset('images/home-all-about-cancer.png') }}" class="float-left mr-3" /> India has a high mortality rate for cancer patients since the detection happens late. Cancer
                            doesn't discriminate between people. Why should you let it win over you? Know more about the
                            disease so that you can fight it better.
                        </p>
                        <!-- <div class="clearfix"></div> -->
                        <p class="mt-3">
                            <a class="btn btn-gray" href="{{ asset('all-about-cancer') }}">KNOW MORE</a>
                        </p>
                    </div>
                    <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-tab-2">
                        <h1 class="title">CANCER SCREENING</h1>
                        <p class="tagline lead mb-1 text-light-gray">Recognising the early signs of cancer is probably the easiest way to empower ourselves against tps
                            dreaded disease.</h2>
                            <div class="short-left-border"></div>
                            <p class="description text-gray">
                                <img width="200" src="{{ asset('images/home-cancer-screening.png') }}" class="float-left mr-3" /> We are here to help you take care of your health. Learn more about the types of cancer screening,
                                how they will help and what is it that you can do. If cancer is detected in the early stages,
                                the chances of survival are higher.
                            </p>
                            <!-- <div class="clearfix"></div> -->
                            <p class="mt-3">
                                <a class="btn btn-gray" href="{{ asset('cancer-screening') }}">KNOW MORE</a>
                            </p>
                    </div>
                    <div class="tab-pane fade" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-tab-3">
                        <h1 class="title">GET INVOLVED</h1>
                        <p class="tagline lead mb-1 text-light-gray">Be a part of this cause, ask us how.</p>
                        <div class="short-left-border"></div>
                        <p class="description text-gray">
                            <img width="200" src="{{ asset('images/home-get-involved1.png') }}" class="float-left mr-3" /> Your health is the biggest asset you have and the change begins with you. The need of the hour
                            is to make people aware about early screening, prevention and cure.
                        </p>
                        <!-- <div class="clearfix"></div> -->
                        <p class="mt-3">
                            <a class="btn btn-gray" href="{{ asset('get-involved') }}">KNOW MORE</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr class="m-0" /> @include('includes.ask-expert-full-form', [])
<div class="section bg-gray-medium pt-5 pb-5 partners-bg">
    <div class="container text-center">
        <div class="row">
            <div class="col-12 mb-3">
                <h2 class="text-primary text-uppercase">HAVE A QUERY?</h2>
                <p>We are here to tell you all that you want to know about Cancer.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="card-deck">
                    <div class="card text-center">
                        <img class="card-img-top" src="{{ asset('images/home-faqs.png') }}" alt="">
                        <div class="card-body">
                            <p class="card-title">
                                <strong>Read FAQs</strong>
                            </p>
                            <p class="card-text">Are these the questions you have been wanting to ask?</p>

                        </div>
                        <div class="card-footer bg-light">
                            <a href="{{ url('faqs') }}" class="btn btn-outline-primary">Read More</a>
                        </div>
                    </div>

                    <div class="card text-center">
                        <img class="card-img-top" src="{{ asset('images/home-contact-us.png') }}" alt="">
                        <div class="card-body">
                            <p class="card-title">
                                <strong>Contact Us</strong>
                            </p>
                            <p class="card-text">Need further help? </p>

                        </div>
                        <div class="card-footer bg-light">
                            <a href="{{ url('contact') }}" class="btn btn-outline-primary">Read More</a>
                        </div>
                    </div>

                    <div class="card text-center">
                        <img class="card-img-top" src="{{ asset('images/ask-an-expert.jpg') }}" alt="">
                        <div class="card-body">
                            <p class="card-title">
                                <strong>Ask An Expert</strong>
                            </p>
                            <p class="card-text">Still have a question? Ask our expert.</p>

                        </div>
                        <div class="card-footer bg-light">
                            <a href="{{ url('/#ask-an-expert-wrapper') }}" class="btn btn-outline-primary">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container pt-5 pb-5 text-center text-light">
        <div class="row">

            <div class="col-12 mb-5">
                <h2>OUR PARTNERS</h2>
            </div>
            <div class="col-6" style="border-right: 1px dotted #ffffff">
                    <p>NGO PARTNERS</p>
                    <div class="row pb-5">
                        <div class="col-sm-12 col-xs-12 col-md-4">
                            <a href="http://www.indiancancersociety.org/" target="_blank"><img class="img-fluid" width="150" height="150" src=" {{  asset('images/ics.jpg') }}" /></a>
                        </div>
                        <div class="col-sm-12 col-xs-12 col-md-4">
                            <a href="https://www.facebook.com/LifeAgainIndia/" target="_blank"><img class="img-fluid" width="150" height="150" src=" {{  asset('images/life-again.jpg') }}" /></a>
                        </div>
                        <div class="col-sm-12 col-xs-12 col-md-4">
                            <a href="http://www.cansupport.org/" target="_blank"><img class="img-fluid" width="150" height="150" src=" {{  asset('images/can-support.jpg') }}" /></a>
                        </div>
                    </div>
                </div>
                <div class="col-6 text-center">
                    <p>HAIR DONATION PARTNERS</p>
                    <div class="row pb-5">
                        <div class="col-sm-12 col-xs-12 col-md-4">
    
                        </div>
                        <div class="col-sm-12 col-xs-12 col-md-4">
    
                            <a href="http://www.naturals.in/our-salons/" target="_blank"><img class="img-fluid" width="150" height="150" src=" {{  asset('images/naturals.jpg') }}" /></a>
                        </div>
                        <div class="col-sm-12 col-xs-12 col-md-4">
    
                        </div>
                    </div>
                </div>
        </div>
        <div class="col-12 pt-5 pb-5">
            <a href="{{ url('partners') }}" class="btn btn-light">KNOW MORE</a>
        </div>
    </div>
</div>

<div class="container pt-5 pb-5">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-4">
            <div class="fb-page" data-href="https://www.facebook.com/TOIMYTIMES/" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/TOIMYTIMES/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/TOIMYTIMES/">My Times</a></blockquote></div>
        </div>
        <div class="col-md-4">
            <a class="twitter-timeline" data-height="580" href="https://twitter.com/TOI_MyTimes?ref_src=twsrc%5Etfw">Tweets by TOI_MyTimes</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
@endsection