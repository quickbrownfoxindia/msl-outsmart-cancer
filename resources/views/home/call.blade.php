@extends('layouts.app') @section('content')
<div class="hero home">


    <div class="item home-change-on-mobile" style="background-image: url('{{ asset('images/home-banner.png') }}'); background-position: top center;">
        <div class="overlay"></div>

        <div class="container h-100">
            <div class="row h-100">
                <div class="col-md-6 col-xs-12 my-auto">
                    <div class="caption">
                        <h1 class="text-uppercase title">The earliest signs of Cancer often come too late</h1>
                        <p>Pledge today and get your Cancer screening done. Give a missed call to
                            <a  class="text-light">8448198134</a>
                        </p>
                        <a data-trigger="ga" data-category="OCP" data-action="call" data-label="call_page" href="tel:8448198134" class="btn btn-light text-primary">
                            <i class="fa fa-phone"></i> Missed Call @ 8448198134</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
@include('includes.navbar-primary')

@endsection