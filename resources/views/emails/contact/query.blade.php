<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Thank you for writing to us!</title>
	<style type="text/css">
		#outlook a {
			padding:0;
		}
		body{
			width:100% !important;
			-webkit-text-size-adjust:100%;
			-ms-text-size-adjust:100%;
			margin:0;
			padding:0;
			font-family: 'Arial', 'Helvetica', sans-serif !important;
		}
		.ExternalClass {
			width:100%;
		}
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
			line-height: 100%;
		}
		#backgroundTable {
			margin:0;
			padding:0;
			width:100% !important;
			line-height: 100% !important;
		}
		img {
			outline:none;
			text-decoration:none;
			-ms-interpolation-mode: bicubic;
		}
		a img {
			border:none;
		}
		.image_fix {
			display:block;
		}
		p {
			margin: 1em 0;
		}
		table td {
			border-collapse: collapse;
		}
		table {
			border-collapse:collapse;
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		@media only screen and (max-device-width: 480px) {
			a[href^="tel"], a[href^="sms"] {
				text-decoration: none;
				pointer-events: none;
				cursor: default;
			}
			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
				text-decoration: default;
				pointer-events: auto;
				cursor: default;
			}
		}
		@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
			a[href^="tel"], a[href^="sms"] {
				text-decoration: none;
				pointer-events: none;
				cursor: default;
			}
			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
				text-decoration: default;
				pointer-events: auto;
				cursor: default;
			}
		}

		@media only screen and (-webkit-min-device-pixel-ratio: 2) {

		}
		@media only screen and (-webkit-device-pixel-ratio:.75){

		}
		@media only screen and (-webkit-device-pixel-ratio:1){

		}
		@media only screen and (-webkit-device-pixel-ratio:1.5){

		}
	</style>
</head>
<body>
	<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" style="background-color: #efefef;">
	<tr>
		<td>
		<table cellpadding="0" cellspacing="0" border="0" align="center">
			<tr>
				<td width="75" valign="top"></td>
				<td width="75" valign="top"></td>
				<td width="75" valign="top"></td>
				<td width="75" valign="top"></td>
				<td width="75" valign="top"></td>
				<td width="75" valign="top"></td>
				<td width="75" valign="top"></td>
				<td width="75" valign="top"></td>
			</tr>

			<tr style="background-color: #efefef;">
				<td width="150" valign="top" colspan="2">
					<a href="https://goo.gl/7Cwzs5" target="_blank" class="image_fix"><img src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/contact_us/camapign-logo.png" alt="Outsmart Cancer" /></a>
				</td>
				<td width="75" valign="top">&nbsp;</td>
				<td width="150" valign="top" colspan="2">
					<a href="https://goo.gl/5sqRMp" target="_blank" class="image_fix"><img src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/contact_us/apollo-logo.png" alt="Apollo Cancer Care Hospitals" /></a>
				</td>
				<td width="75" valign="top">&nbsp;</td>
				<td width="150" valign="top" colspan="2">
					<a href="https://goo.gl/XmQoUX" target="_blank" class="image_fix"><img src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/contact_us/toi-logo.png" alt="The Times of India" /></a>
				</td>
			</tr>

			<tr>
				<td width="600" valign="top" colspan="8">
					<img class="image_fix" src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/contact_us/banner.png" alt="Outsmart Cancer by Apollo Cancer Care Hospitals and The Times of India" />
				</td>
			</tr>

			<tr>
				<td width="75" valign="top">
					<img style="width:75px;height:280px;" class="image_fix" src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/contact_us/shadow-left.png" />
				</td>
				<td width="450" valign="top" colspan="6" style="background-color: #fff;line-height:1.3;font-size:13px;">
					<p style="margin-left:30px;margin-right:30px;">Hello there,</p>
					<p>&nbsp;</p>
					<p style="margin-left:30px;margin-right:30px;">Thank you for reaching out to us with your query. We will get in touch with you soon.</p>
					<p style="margin-left:30px;margin-right:30px;">To know more about the Outsmart Cancer Awareness initiative, please visit our <strong><a href="https://goo.gl/7Cwzs5" target="_blank" style="color:#a7248b;">website</a></strong> </p>
					<p>&nbsp;</p>
					<p style="margin-left:30px;margin-right:30px;text-align:right;">Thank you,<br/>
					Team Outsmart Cancer<br/>
					An initiative by The Times of India<br/>
					Presented by <a href="https://www.apollohospitals.com/" target="_blank" style="color:#333;text-decoration:none;">Apollo Cancer Centre</a></p>
				</td>
				<td width="75" valign="top">
					<img style="width:75px;height:280px;" class="image_fix" src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/contact_us/shadow-right.png" />
				</td>
			</tr>

			<tr>
				<td width="225" valign="top" colspan="3">
					<img class="image_fix" src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/contact_us/shadow-bottom-left.png" />
				</td>
				<td width="75" valign="top">
					<a href="https://goo.gl/MHWTGB" target="_blank"><img class="image_fix" src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/contact_us/facebook.png" alt="Facebook" /></a>
				</td>
				<td width="75" valign="top">
					<a href="https://goo.gl/kwc5tP" target="_blank"><img class="image_fix" src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/contact_us/twitter.png" alt="Twitter" /></a>
				</td>
				<td width="225" valign="top" colspan="3">
					<img class="image_fix" src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/contact_us/shadow-bottom-right.png" />
				</td>
			</tr>

			<tr style="background-color:#efefef;text-align:center;font-size:12px;">
				<td width="600" valign="top" colspan="8">
					<center><p style="margin-top:25px;margin-bottom:25px;">You're receiving this automated email because you registered on our website.</p></center>
				</td>
			</tr>

			<tr>
				<td width="75" valign="top"></td>
				<td width="75" valign="top"></td>
				<td width="75" valign="top"></td>
				<td width="75" valign="top"></td>
				<td width="75" valign="top"></td>
				<td width="75" valign="top"></td>
				<td width="75" valign="top"></td>
				<td width="75" valign="top"></td>
			</tr>
		</table>
		</td>
	</tr>
	</table>
</body>
</html>
