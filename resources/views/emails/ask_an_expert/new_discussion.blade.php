<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Our audience have asked their question(s)</title>
	<style type="text/css">#outlook a {
			padding:0;
		}
		body{
			width:100% !important;
			-webkit-text-size-adjust:100%;
			-ms-text-size-adjust:100%;
			margin:0;
			padding:0;
			font-family: 'Arial', 'Helvetica', sans-serif !important;
		}
		.ExternalClass {
			width:100%;
		}
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
			line-height: 100%;
		}
		#backgroundTable {
			margin:0;
			padding:0;
			width:100% !important;
			line-height: 100% !important;
		}
		img {
			outline:none;
			text-decoration:none;
			-ms-interpolation-mode: bicubic;
		}
		a img {
			border:none;
		}
		.image_fix {
			display:block;
		}
		p {
			margin: 1em 0;
		}
		table td {
			border-collapse: collapse;
		}
		table {
			border-collapse:collapse;
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		@media only screen and (max-device-width: 480px) {
			a[href^="tel"], a[href^="sms"] {
				text-decoration: none;
				pointer-events: none;
				cursor: default;
			}
			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
				text-decoration: default;
				pointer-events: auto;
				cursor: default;
			}
		}
		@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
			a[href^="tel"], a[href^="sms"] {
				text-decoration: none;
				pointer-events: none;
				cursor: default;
			}
			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
				text-decoration: default;
				pointer-events: auto;
				cursor: default;
			}
		}

		@media only screen and (-webkit-min-device-pixel-ratio: 2) {

		}
		@media only screen and (-webkit-device-pixel-ratio:.75){

		}
		@media only screen and (-webkit-device-pixel-ratio:1){

		}
		@media only screen and (-webkit-device-pixel-ratio:1.5){

		}
	</style>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" id="backgroundTable" style="background-color: #efefef;">
	<tbody>
		<tr>
			<td>
			<table align="center" border="0" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td valign="top" width="75"> </td>
						<td valign="top" width="75"> </td>
						<td valign="top" width="75"> </td>
						<td valign="top" width="75"> </td>
						<td valign="top" width="75"> </td>
						<td valign="top" width="75"> </td>
						<td valign="top" width="75"> </td>
						<td valign="top" width="75"> </td>
					</tr>
					<tr style="background-color: #efefef;">
						<td colspan="2" valign="top" width="150"><a class="image_fix" href="https://goo.gl/7Cwzs5" target="_blank"><img alt="Outsmart Cancer" src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/question/camapign-logo.png" /></a></td>
						<td valign="top" width="75"> </td>
						<td colspan="2" valign="top" width="150"><a class="image_fix" href="https://goo.gl/5sqRMp" target="_blank"><img alt="Apollo Cancer Care Hospitals" src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/question/apollo-logo.png" /></a></td>
						<td valign="top" width="75"> </td>
						<td colspan="2" valign="top" width="150"><a class="image_fix" href="https://goo.gl/XmQoUX" target="_blank"><img alt="The Times of India" src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/question/toi-logo.png" /></a></td>
					</tr>
					<tr>
						<td colspan="8" valign="top" width="600"><img alt="Outsmart Cancer by Apollo Cancer Care Hospitals and The Times of India" class="image_fix" src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/question/banner.png" style="width:100%;" /></td>
					</tr>
					<tr>
						<td valign="top" width="75"><img class="image_fix" src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/question/shadow-left.png" style="width:75px;height:315px;" /></td>
						<td colspan="6" style="background-color: #fff;line-height:1.3;font-size:13px;" valign="top" width="450">
						<p style="margin-left:30px;margin-right:30px;">Hello,</p>

						<p> </p>

						<p style="margin-left:30px;margin-right:30px;">This is to let you know that we have received a query from {{ $discussion->user()->first()->name }}:-</p>

						<p style="margin-left:30px;margin-right:30px;"> {{ strip_tags($post->body) }}</p>

						<p style="margin-left:30px;margin-right:30px;">Please visit <a href="{{ url('/') }}/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.discussion') }}/{{ $discussion->category->slug }}/{{ $discussion->slug }}" target="_blank">link</a> to answer the same. </p>

						<p> </p>

						<p> </p>

						<p style="margin-left:30px;margin-right:30px;text-align:right;">Thank you,<br />
						Team Outsmart Cancer<br />
						An initiative by The Times of India<br />
						Presented by <a href="https://www.apollohospitals.com/ " target="_blank">Apollo Cancer Centre</a></p>
						</td>
						<td valign="top" width="75"><img class="image_fix" src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/question/shadow-right.png" style="width:75px;height:315px;" /></td>
					</tr>
					<tr>
						<td colspan="3" valign="top" width="225"><img class="image_fix" src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/question/shadow-bottom-left.png" /></td>
						<td valign="top" width="75"><a href="https://goo.gl/MHWTGB" target="_blank"><img alt="Facebook" class="image_fix" src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/question/facebook.png" /></a></td>
						<td valign="top" width="75"><a href="https://goo.gl/kwc5tP" target="_blank"><img alt="Twitter" class="image_fix" src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/question/twitter.png" /></a></td>
						<td colspan="3" valign="top" width="225"><img class="image_fix" src="https://s3.amazonaws.com/toi-outsmart-cancer/emailers/question/shadow-bottom-right.png" /></td>
					</tr>
					<tr style="background-color:#efefef;text-align:center;font-size:12px;">
						<td colspan="8" valign="top" width="600">
						<center>
						<p style="margin-top:25px;margin-bottom:25px;">You're receiving this automated email because you registered on our website.</p>
						</center>
						</td>
					</tr>
					<tr>
						<td valign="top" width="75"> </td>
						<td valign="top" width="75"> </td>
						<td valign="top" width="75"> </td>
						<td valign="top" width="75"> </td>
						<td valign="top" width="75"> </td>
						<td valign="top" width="75"> </td>
						<td valign="top" width="75"> </td>
						<td valign="top" width="75"> </td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>
