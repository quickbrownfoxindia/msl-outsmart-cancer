@extends('layouts.app') @section('content')
<div class="hero" data-trigger="slick">

    <div class="item" style="background-image: url('{{ asset('images/get-involved/banner.jpg') }}'); background-position: center center;">
        <div class="overlay"></div>

        <div class="container h-100">
            <div class="row  h-100">
                <div class="col-md-6 col-xs-12 my-auto">
                    <div class="caption">
                        <h1 class="text-uppercase title">Be a part of the cause </h1>
                        <p> Join us today to create a healthier tomorrow.</p>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

@include('includes.navbar-primary')

<div class="section bg-white pt-5 pb-5">
    
    <div class="container text-center">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-pills nav-justified" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="tab1" data-toggle="tab" href="#tab-1" role="tab" aria-controls="tab-1" aria-selected="true">
                            <img class="img-fluid mb-3" src="{{ asset('images/get-involved/hair.jpg') }}" />
                            <h5 class="text-uppercase text-left">Hair Donation</h5><br/>
                            <div class="row">
                                <div class="col">
                                    <i class="fa fa-angle-down pull-left fa-2x"></i>
                                </div>
                                <div class="col">
                                    <i class="fa fa-angle-up pull-right fa-2x"></i>
                                </div>
                            </div>
                            <p class="small text-left d-none d-sm-block">Only 6 inches of your hair and infinite smiles for someone.</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab4" data-toggle="tab" href="#tab-4" role="tab" aria-controls="tab-4" aria-selected="false">
                            <img class="img-fluid mb-3" src="{{ asset('images/get-involved/talks.jpg') }}" />
                            <h5 class="text-uppercase text-left">Cancer Talks</h5><br/>
                            <div class="row">
                                <div class="col">
                                    <i class="fa fa-angle-down pull-left fa-2x"></i>
                                </div>
                                <div class="col">
                                    <i class="fa fa-angle-up pull-right fa-2x"></i>
                                </div>
                            </div>
                            <p class="small text-left d-none d-sm-block">Cancer workshops across India.</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab2" data-toggle="tab" href="#tab-2" role="tab" aria-controls="tab-2" aria-selected="false">
                            <img class="img-fluid mb-3" src="{{ asset('images/get-involved/walk.jpg') }}" />
                            <h5 class="text-uppercase text-left">#OneCroreSteps Walk Against Cancer</h5>
                            <div class="row">
                                <div class="col">
                                    <i class="fa fa-angle-down pull-left fa-2x"></i>
                                </div>
                                <div class="col">
                                    <i class="fa fa-angle-up pull-right fa-2x"></i>
                                </div>
                            </div>
                            <p class="small text-left d-none d-sm-block"> Join us in the walk of  1 crore steps against Cancer across major cities.</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab3" data-toggle="tab" href="#tab-3" role="tab" aria-controls="tab-3" aria-selected="false">
                            <img class="img-fluid mb-3" src="{{ asset('images/zumbathon.png') }}" />
                            <h5 class="text-uppercase text-left">Zumbathon</h5><Br/>
                            <div class="row">
                                <div class="col">
                                    <i class="fa fa-angle-down pull-left fa-2x"></i>
                                </div>
                                <div class="col">
                                    <i class="fa fa-angle-up pull-right fa-2x"></i>
                                </div>
                            </div>
                            <p class="small text-left d-none d-sm-block">Dance for a healthier future.</p>
                        </a>
                    </li>
                    <li class="nav-item d-none d-sm-block">

                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active text-left pt-3" id="tab-1" role="tabpanel" aria-labelledby="tab-1">

                        <p>To pledge your support for cancer survivors, donate 6 inches of your hair at the nearest Naturals salon. Your donation will be used to make a wig for a Cancer patient. In return, you not only get blessings but also a clippable purple streak as your badge of honor. Locate the nearest Naturals salon <a target="_blank" href="http://www.naturals.in/our-salons/">here</a>.<br/>
                        <img class="img-fluid" width="200" src="{{ asset('images/naturals.jpg') }}" />
                        </p>


                    </div>
                    <div class="tab-pane fade text-left pt-3" id="tab-2" aria-labelledby="tab-2">
                        <p>Every step you take will count. Come be a part of our walk towards a cancer-aware and informed community which knows the importance of early screening.</p>
                        <p><strong>Ahmedabad</strong>

<p>Date: 27th January</br>
Time: 4pm</br>
Venue: Eklavya Sports Academy</p>

<p>To register, SMS OUTSMART&lt;space&gt;AHMEDABAD TO 58888</p>

<p><strong>Hyderabad</strong></p>

<p>Date: 27th January</br>
Time: 4pm</br>
Venue: Gachibowli Stadium</p>

<p>To register, SMS OUTSMART&lt;space&gt;HYDERABAD TO 58888</p>

<p><strong>Bangalore</strong></p>

<p>Date: 3rd February</p>
Time: 10:30 am</br>
Venue: M.S. Ramaiah Institute of Technology</p>

<p>To register, SMS OUTSMART&lt;space&gt;BANGALORE TO 58888</p>

<p><strong>Kolkata</strong></p>

<p>Date: 3rd February</br>
Time: 3:30 pm onwards</br>
Venue: Gokhale Memorial Girls' College</p>

<p>To register, SMS OUTSMART&lt;space&gt;KOLKATA TO 58888</p>

<p><strong>Mumbai</strong></p>

<p>Date: 2nd February</br>
Time: 10:30 am onwards</br>
Venue: SNDT Women’s University – Juhu Road</p>

<p>To register, SMS OUTSMART&lt;space&gt;MUMBAI TO 58888</p>

 
<p><strong>Chennai</strong></p>
<p>Date: 4th Feb</br>
Time: 7:30 AM</br>
Venue: Olcott Higher Secondary School, BESANT Nagar</p>

<p>To register, SMS OUTSMART&lt;space&gt;CHENNAI TO 58888</p>

<p><strong>Delhi</strong></p>

<p>Date: 4th February</br>
Time: 8am – 9am</br>
Venue: Rajpath, Raisina Hill, India Gate</p>

<p>To register, SMS OUTSMART<space>DELHI TO 5888</p>

<iframe src="https://www.google.com/maps/d/embed?mid=16W4GL6B3LXfbGfh_Lv63cebCkaWDShZh&ll=21.010857788063877%2C80.4335231&z=5" style="width: 100%" height="480"></iframe>


                    </div>
                    <div class="tab-pane fade text-left pt-3" id="tab-3" aria-labelledby="tab-3">
                        <p>Make moves for a healthy cause by being a part of our Zumbathon and taking a pledge today in the fight against Cancer.</p>
                        <p><strong>Ahmedabad</strong>

<p>Date: 27th January</br>
Time: 4pm</br>
Venue: Eklavya Sports Academy</p>

<p>To register, SMS OUTSMART&lt;space&gt;AHMEDABAD TO 58888</p>

<p><strong>Hyderabad</strong></p>

<p>Date: 27th January</br>
Time: 4pm</br>
Venue: Gachibowli Stadium</p>

<p>To register, SMS OUTSMART&lt;space&gt;HYDERABAD TO 58888</p>

<p><strong>Bangalore</strong></p>

<p>Date: 3rd February</p>
Time: 10:30 am</br>
Venue: M.S. Ramaiah Institute of Technology</p>

<p>To register, SMS OUTSMART&lt;space&gt;BANGALORE TO 58888</p>

<p><strong>Kolkata</strong></p>

<p>Date: 3rd February</br>
Time: 3:30 pm onwards</br>
Venue: Gokhale Memorial Girls' College</p>

<p>To register, SMS OUTSMART&lt;space&gt;KOLKATA TO 58888</p>

<p><strong>Mumbai</strong></p>

<p>Date: 2nd February</br>
Time: 10:30 am onwards</br>
Venue: SNDT Women’s University – Juhu Road</p>

<p>To register, SMS OUTSMART&lt;space&gt;MUMBAI TO 58888</p>

 
<p><strong>Chennai</strong></p>
<p>Date: 4th Feb</br>
Time: 7:30 AM</br>
Venue: Olcott Higher Secondary School, BESANT Nagar</p>

<p>To register, SMS OUTSMART&lt;space&gt;CHENNAI TO 58888</p>

<p><strong>Delhi</strong></p>
<p>Date: 4th February</br>
Time: 8am – 9am</br>
Venue: Rajpath, Raisina Hill, India Gate</p>

<p>To register, SMS OUTSMART<space>DELHI TO 5888</p>

<iframe src="https://www.google.com/maps/d/embed?mid=16W4GL6B3LXfbGfh_Lv63cebCkaWDShZh&ll=21.010857788063877%2C80.4335231&z=5" style="width: 100%" height="480"></iframe>
                    </div>
                    <div class="tab-pane fade text-left pt-3" id="tab-4" aria-labelledby="tab-4">

                <div class="card-deck">
                        <div class="card text-center">
                            <img class="card-img-top" src="{{ asset('images/get-involved/Delhi.jpeg') }}" alt="">
                            <div class="card-body">
                                <p class="card-title">
                                    <strong>Cancer Talks in Delhi</strong>
                                </p>
                                <p class="card-text">Cancer talks were organized at various institutes in Delhi by various experts from CanSupport and doctors from Apollo Hospitals as part of the #OutsmartCancer initiative of the The Times of India</p>

                            </div>
                            <div class="card-footer bg-light">
                                <a target="_blank" href="{{ url('https://www.facebook.com/pg/TOIMYTIMES/photos/?tab=album&album_id=1443815419061390') }}" class="btn btn-outline-primary">Read More</a>
                            </div>
                        </div>

                        <div class="card text-center">
                            <img class="card-img-top" src="{{ asset('images/get-involved/Mumbai.jpg') }}" alt="">
                            <div class="card-body">
                                <p class="card-title">
                                    <strong>Cancer Talks in Mumbai</strong>
                                </p>
                                <p class="card-text">Cancer talks were organized at various institutes in Mumbai by various experts from CanSupport and doctors from Apollo Hospitals as part of the #OutsmartCancer initiative of the The Times of India</p>

                            </div>
                            <div class="card-footer bg-light">
                                <a target="_blank" href="{{ url('https://www.facebook.com/pg/TOIMYTIMES/photos/?tab=album&album_id=1443818595727739') }}" class="btn btn-outline-primary">Read More</a>
                            </div>
                        </div>

                        <div class="card text-center">
                            <img class="card-img-top" src="{{ asset('images/get-involved/Kolkata.jpeg') }}" alt="">
                            <div class="card-body">
                                <p class="card-title">
                                    <strong>Cancer Talks in Kolkata</strong>
                                </p>
                                <p class="card-text">Cancer talks were organized at various institutes in Kolkata by various experts from CanSupport and doctors from Apollo Hospitals as part of the #OutsmartCancer initiative of the The Times of India</p>

                            </div>
                            <div class="card-footer bg-light">
                                <a target="_blank" href="{{ url('https://www.facebook.com/pg/TOIMYTIMES/photos/?tab=album&album_id=1443827585726840') }}" class="btn btn-outline-primary">Read More</a>
                            </div>
                        </div>

                        <div class="card text-center">
                            <img class="card-img-top" src="{{ asset('images/get-involved/Chennai.jpeg') }}" alt="">
                            <div class="card-body">
                                <p class="card-title">
                                    <strong>Cancer Talks in Chennai</strong>
                                </p>
                                <p class="card-text">Cancer talks were organized at various institutes in Chennai by various experts from CanSupport and doctors from Apollo Hospitals as part of the #OutsmartCancer initiative of the The Times of India</p>

                            </div>
                            <div class="card-footer bg-light">
                                <a target="_blank" href="{{ url('https://www.facebook.com/pg/TOIMYTIMES/photos/?tab=album&album_id=1443824269060505') }}" class="btn btn-outline-primary">Read More</a>
                            </div>
                        </div>
                        <div class="card text-center">
                            <img class="card-img-top" src="{{ asset('images/get-involved/Chennai.jpeg') }}" alt="">
                            <div class="card-body">
                                <p class="card-title">
                                    <strong>Cancer Talks in Bangalore</strong>
                                </p>
                                <p class="card-text">Cancer talks were organized at various institutes and organizations in Bangalore by various experts from Indian Cancer Society and Life Again India and doctors from Apollo Hospitals as part of the #OutsmartCancer initiative of The Times of India.</p>

                            </div>
                            <div class="card-footer bg-light">
                                <a target="_blank" href="{{ url('https://www.facebook.com/pg/TOIMYTIMES/photos/?tab=album&album_id=1455997911176474 ') }}" class="btn btn-outline-primary">Read More</a>
                            </div>
                        </div>

                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <hr class="m-0" /> @include('includes.spread-the-word')
    <hr class="m-0" /> @include('includes.our-partners', ['extraClass' => 'bg-gray-light text-light'])
    <hr class="m-0" /> @endsection