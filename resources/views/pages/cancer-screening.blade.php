@extends('layouts.app') @section('content')
<div class="hero" data-trigger="slick">

    <div class="item" style="background-image: url('{{ asset('images/cancer-screening/banner.jpg') }}'); background-position: center center;">
        <div class="overlay"></div>

        <div class="container h-100">
            <div class="row h-100">
                <div class="col-md-6 col-xs-12 my-auto">
                    <div class="caption">
                        <h1 class="text-uppercase title">Cancer Screening</h1>
                        <p>Recognising the early signs of Cancer is probably the easiest way to empower ourselves against the dreadful disease.</p>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

@include('includes.navbar-primary')

<div class="section bg-white pt-5 pb-5">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-pills nav-justified" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="tab1" data-toggle="tab" href="#tab-1" role="tab" aria-controls="tab-1" aria-selected="true">
                            <img class="img-fluid mb-3" src="{{ asset('images/cancer-screening/screen.jpg') }}" />
                            <h5 class="text-uppercase text-left">Why do you need Cancer Screening?</h5>
                            <div class="row">
                                <div class="col">
                                    <i class="fa fa-angle-down pull-left fa-2x"></i>
                                </div>
                                <div class="col">
                                    <i class="fa fa-angle-up pull-right fa-2x"></i>
                                </div>
                            </div>
                            <p class="small text-left d-none d-sm-block">The incidence of Cancer rate is going up and there is a dire need to fight the evil. </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab2" data-toggle="tab" href="#tab-2" role="tab" aria-controls="tab-2" aria-selected="false">
                            <img class="img-fluid mb-3" src="{{ asset('images/cancer-screening/tests.jpg') }}" />
                            <h5 class="text-uppercase text-left">Types of Cancer Screening Tests</h5>
                            <div class="row">
                                <div class="col">
                                    <i class="fa fa-angle-down pull-left fa-2x"></i>
                                </div>
                                <div class="col">
                                    <i class="fa fa-angle-up pull-right fa-2x"></i>
                                </div>
                            </div>
                            <p class="small text-left d-none d-sm-block">Tests for screening of different types of cancers.</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab3" data-toggle="tab" href="#tab-3" role="tab" aria-controls="tab-3" aria-selected="false">
                            <img class="img-fluid mb-3" src="{{ asset('images/cancer-screening/freq.jpg') }}" />
                            <h5 class="text-uppercase text-left">How often should you screen for Cancer? </h5>
                            <div class="row">
                                <div class="col">
                                    <i class="fa fa-angle-down pull-left fa-2x"></i>
                                </div>
                                <div class="col">
                                    <i class="fa fa-angle-up pull-right fa-2x"></i>
                                </div>
                            </div>
                            <p class="small text-left d-none d-sm-block">Cancer screening frequency for different types of cancers based on age.</p>
                        </a>
                    </li>
                    <li class="nav-item d-none d-sm-block">

                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active text-left pt-3" id="tab-1" role="tabpanel" aria-labelledby="tab-1">

                        <p>Cancer in India is poised to breach the ‘one million patients a year’ mark and of those diagnosed
                            annually, only 30% have been statistically proven to beat the disease. With the incidence rate
                            going up, cancer is an insidious enemy that needs to be met head on.</p>
                        <p>The silver lining is that cancer is effectively countered if detected early; empirical evidence from
                            across the world reinforces this finding. The incidence of cancerous cells is always contained
                            at the early stage, and this increases the efficacy of treatment methods immensely. </p>
                        <p>Universally, early detection of cancer is a function of two key parameters – the level of education
                            about the symptoms of cancer, and the prevalence of cancer screenings. </p>


                    </div>
                    <div class="tab-pane fade text-left pt-3" id="tab-2" aria-labelledby="tab-2">
                        <p>Cancer screening involves efforts to detect pre-malignant conditions in the body (i.e. conditions
                            which may proceed to cancer if not treated). It also involves detecting cancers after it has
                            formed, but before any noticeable symptoms appear (Unlike diagnostic efforts prompted by symptoms
                            and medical signs). This may involve physical examination, blood or urine tests or medical imaging.
                        </p>
                        <p>Here are the types of tests for different cancers:</p>
                        <ul>
                            <li>Breast Cancer screening for all women above the age of 30 years with (Clinical Breast Examination,
                                Self Breast Examination and Mammography for women above 50 years of age)</li>
                            <li>Cervical Cancer screening in women who are sexually active. Precancerous changes in the cervix
                                often do not cause any signs or symptoms. Pap smear test and human papilloma virus testing
                                are done to screen for Cervical Cancer</li>
                            <li>Oral Cancer screening in tobacco users.</li>
                            <li>Colonoscopy/ sigmoidoscopy for Screening for Colorectal Cancer</li>
                            <li>Low dose CT Scan to screen for Lung Cancer in those who are at high risk of Lung Cancer:
                                <ul>
                                    <li>Age 50 - 74yrs</li>
                                    <li>Have a history of heavy smoking -30 packs/ year*</li>
                                    <li>Are still smoking or have quit in last 15yrs</li>
                                    <li>*(30 packs/ year means one packet of cigarettes/day for 30 years or 2 packet/day for
                                        15 years)</li>
                                </ul>
                            </li>
                            <li>Prostate specific antigen test is done to detect Prostate Cancer at an early stage. However it
                                is no longer recommended as a screening test as it leads to overdiagnosis and overtreatment
                                and has little effect on Prostate Cancer deaths.
                            </li>
                        </ul>
                    </div>
                    <div class="tab-pane fade text-left pt-3" id="tab-3" aria-labelledby="tab-3">
                        <p>How often should you screen for Cancer?</p>
                        <ul>
                            <li>Breast Cancer screening guidelines for Women
                                <ul>
                                    <li>
                                        Age-wise distinction for tests:-
                                        <ul>
                                            <li>20-40 yrs</li>
                                            <ul>
                                                <li>Clinical breast examination every 3 years</li>
                                                <li>
                                                    Breast self-examination
                                                </li>
                                            </ul>
                                            <li>
                                                >40 yrs-50yrs
                                                <ul>
                                                    <li>Clinical breast examination every year</li>
                                                    <li>Mammography yearly till 50 yrs age</li>
                                                </ul>


                                            </li>
                                            <li>
                                                >50 yrs
                                                <ul>
                                                    <li>Mammography every 2 yrs</li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                Women at increased risk of Breast Cancer
                                <ul>
                                    <li>Age-wise distinction for tests:
                                        <ul>
                                            <li>
                                                In these women mammography should start at 35 yrs
                                                <ul>
                                                    <li>Women who have one or more blood relations who have had breast cancer</li>
                                                    <li>Women who have a number of blood relatives who have had ovarian cancer
                                                        and breast cancer</li>
                                                    <li>Women who have had radiation to the chest wall</li>

                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section bg-white pt-5 pb-5">

<div class="container">
    <div class="row">
        <div class="col-12">
            <p>To view the Cancer Screening packages available at Apollo Hospitals, click <a href="https://www.askapollo.com/physical-appointment/BookHealthCheckAppointment.aspx" target="_blank">here</a>.</p>
        </div>
    </div>
</div>

</div>
    <hr class="m-0" /> @include('includes.spread-the-word')
    <hr class="m-0" /> @include('includes.our-partners', ['extraClass' => 'bg-gray-light text- partners-bg text-light'])
    <hr class="m-0" /> @endsection