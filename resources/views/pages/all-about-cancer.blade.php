@extends('layouts.app') @section('content')
<div class="hero" data-trigger="slick">

    <div class="item" style="background-image: url('{{ asset('images/all-about-cancer/banner.jpg') }}'); background-position: center center;">
        <div class="overlay"></div>

        <div class="container h-100">
            <div class="row h-100">
                <div class="col-md-6 col-xs-12 my-auto">
                    <div class="caption">
                        <h1 class="text-uppercase title">ALL ABOUT CANCER</h1>
                        <p>Here’s all you need to know about Cancer including its types, causes, symptoms, stages and treatment.</p>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

@include('includes.navbar-primary')

<div class="section bg-white pt-5 pb-5">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-12">
                
                <ul class="nav nav-pills nav-justified" id="myTab" role="tablist">
                    <li class="nav-item active">
                        <a class="nav-link active" id="tab1" data-toggle="tab" href="#tab-1" role="tab" aria-controls="tab-1" aria-selected="true">
                            <img class="img-fluid mb-3" src="{{ asset('images/all-about-cancer/what.jpg') }}" />
                            <h5 class="text-uppercase text-left">WHAT IS CANCER?</h5>
                            
                            <div class="row">
                                <div class="col">
                                    <i class="fa fa-angle-down pull-left fa-2x"></i>
                                </div>
                                <div class="col">
                                    <i class="fa fa-angle-up pull-right fa-2x"></i>
                                </div>
                            </div>
                            
                            <p class="small text-left d-none d-sm-block">It's time to get to know cancer.</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab2" data-toggle="tab" href="#tab-2" role="tab" aria-controls="tab-2" aria-selected="false">
                            <img class="img-fluid mb-3" src="{{ asset('images/all-about-cancer/types.jpg') }}" />
                            <h5 class="text-uppercase text-left">Types of Cancer </h5>
                            
                            <div class="row">
                                <div class="col">
                                    <i class="fa fa-angle-down pull-left fa-2x"></i>
                                </div>
                                <div class="col">
                                    <i class="fa fa-angle-up pull-right fa-2x"></i>
                                </div>
                            </div>
                            
                            <p class="small text-left d-none d-sm-block">The different types of Cancers and their categorisation.</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab3" data-toggle="tab" href="#tab-3" role="tab" aria-controls="tab-3" aria-selected="false">
                            <img class="img-fluid mb-3" src="{{ asset('images/all-about-cancer/stages.jpg') }}" />
                            <h5 class="text-uppercase text-left">Stages of Cancer</h5>
                            
                            <div class="row">
                                <div class="col">
                                    <i class="fa fa-angle-down pull-left fa-2x"></i>
                                </div>
                                <div class="col">
                                    <i class="fa fa-angle-up pull-right fa-2x"></i>
                                </div>
                            </div>
                            
                            <p class="small text-left d-none d-sm-block">Know the different stages of Cancer based on its extent and severity.</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab4" data-toggle="tab" href="#tab-4" role="tab" aria-controls="tab-4" aria-selected="false">
                            <img class="img-fluid mb-3" src="{{ asset('images/all-about-cancer/treat.jpg') }}" />
                            <h5 class="text-uppercase text-left">Cancer Treatment</h5>
                            
                            <div class="row">
                                <div class="col">
                                    <i class="fa fa-angle-down pull-left fa-2x"></i>
                                </div>
                                <div class="col">
                                    <i class="fa fa-angle-up pull-right fa-2x"></i>
                                </div>
                            </div>
                            
                            <p class="small text-left d-none d-sm-block">There are various types of cancer treatment. Know about them.</p>
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active text-left pt-3" id="tab-1" role="tabpanel" aria-labelledby="tab-1">

                        <p>The cell is body’s basic building block. Cancer is a disease of the cells and occurs when abnormal cells
                            grow in an uncontrolled way. These abnormal cells can damage or invade the surrounding tissues,
                            or spread to other parts of the body and grow there. Most cancers start in a particular organ;
                            this is called the primary site or primary tumour. Tumours can be benign (not cancer) or malignant
                            (cancer).
                        </p>
                        <h5>What is Benign tumor?</h5>
                        <p>Benign tumours do not spread outside their normal boundary to other parts of the body. Some benign
                            tumours are precancerous and may progress to cancer if left untreated. However, if a benign tumour
                            continues to grow at the original site, it can cause a problem by pressing on nearby organs.</p>
                        <h5>What is Malignant tumor?</h5>
                        <p>A malignant tumour is made up of cancer cells. When it first develops, this malignant tumour may
                            be confined to its original site. This is known as a cancer in situ (or carcinoma in situ). If
                            these cells are not treated, they may spread beyond their normal boundaries through path of least
                            resistance into the surrounding tissues. It can also spread by lymph or blood stream to other
                            organs this is called metastasis.</p>
                        <h5>Probable Causes of Cancer</h5>
                        <p>For most cancers the causes are not fully understood. However, some factors place individuals at
                            a greater risk for cancer are well-recognized. Examples include:</p>
                        <ul>
                            <li>Tobacco smoking</li>
                            <li>Alcohol consumption</li>
                            <li>Diet - for example, high intake of particular foods (such as processed meat and foods that have
                                high salt content and fats) overweight and obesity UV radiation</li>
                            <li>Infections –e.g. some types of human papillomavirus (HPV) infection can be associated with cervical
                                and other cancers, and chronic hepatitis B or C infection can be associated with liver cancer</li>
                            <li>Occupational exposure to agents, including chemicals, dusts and industrial processes</li>
                            <li>Family history and genetic susceptibility - some genes that can predispose a person to cancer
                                can be passed on from parent to child</li>
                        </ul>
                        <h5>How does cancer spread? </h5>
                        <p>When cancer cells break away from a tumor, they can travel to other parts of the body through the
                            bloodstream or the lymph system. (Lymph vessels are much like blood vessels, except they carry
                            a clear fluid and immune system cells.)</p>


                    </div>
                    <div class="tab-pane fade text-left pt-3" id="tab-2" aria-labelledby="tab-2">
                        <!-- <p>Cancer can be grouped into several broad categories based on the cell they originate from:- </p>
                        <ul>
                            <li>Carcinoma: This starts from the skin or in tissues that line or cover internal organs (epithelium).
                                Most of the common cancers which arise in the breast, ovary, lung, colon and mouth are carcinomas.</li>
                            <li>Sarcoma: This begins in bone, fat, muscle, blood vessel, or other supportive or connective tissue.</li>
                            <li>Lymphoma and myeloma: Cancers that start in cells of the immune system.</li>
                            <li>Central nervous system cancer: This begins in the brain or spinal cord.</li>
                            <li>Leukaemia: Cancer that begins in the tissues that make blood cells, such as the bone marrow.</li>
                        </ul> -->
                        @include('includes.infographic')
                    </div>
                    <div class="tab-pane fade text-left pt-3" id="tab-3" aria-labelledby="tab-3">
                        <p>Staging helps doctors plan treatment and predict outlook or prognosis.</p>
                        <!-- <p>
                            <strong>Stage 1:</strong> Cancer is small and has not spread outside the host organ. it started in.</p>
                        <p>
                            <strong>Stage 2:</strong> Cancer is larger than stage 1 but has still not spread outside to other organs.
                            it started in. Cancer cells may have spread to lymph nodes near the tumor.</p>
                        <p>
                            <strong>Stage 3:</strong> Cancer is much larger and may have started to spread into surrounding tissues.
                            Cancer cells have spread to the lymph nodes of that area.</p>
                        <p>
                            <strong>Stage 4:</strong> Cancer has spread from where it started to another to other organs. This is
                            known as metastasis</p> -->
<p>                            <img class="img-fluid" src="{{ asset('images/stages-of-cancer.jpg') }}" /></p>
                    </div>
                    <div class="tab-pane fade text-left pt-3" id="tab-4" aria-labelledby="tab-4">
                        <p>The types of treatment that patient receives will depend on the type of cancer the patient has and
                            how advanced it is. The main types of cancer treatment include: </p>
                        <ol>
                            <li>Surgery: Usually the “cancer affected “part is removed/ excised along with the neighbouring structures-
                                if invaded. </li>
                            <li>Radiation Therapy: This type of treatment uses high energy particles or waves to destroy or damage
                                cancer cells. Radiation can be given alone or along with other treatments such as surgery
                                / chemotherapy. </li>
                            <li>Chemotherapy: Chemotherapy is the usage of chemicals (medicines) to treat (by destroying or damaging)
                                cancer cells.</li>
                            <li>Immunotherapy: Immunotherapy is a form of treatment that uses the body’s immune system to fight
                                the cancer which can be done in a few ways depending on the choice of your physician such
                                as : stimulating your own immune cells to attack the cancer cells or giving you immune system
                                components (man-made proteins).</li>
                            <li>Hormone Therapy: Hormone therapy is using systemic (whole body) treatment for hormone receptive
                                cancers such as breast cancer. </li>
                            <li>Stem Cell Transplant: Stem cell transplant is a type of treatment used for certain kinds of cancer
                                such as leukemia and multiple myeloma. Bone marrow contains the hematopoietic stem cells
                                which are used in the treatment. Some people with cancer will have only one treatment but
                                most people have a combination of treatments, such as surgery with chemotherapy and/or radiation
                                therapy. When you need treatment for cancer, you have a lot to learn and think about. It
                                is normal to feel overwhelmed and confused. But, talking with your doctor and learning about
                                the types of treatment you may have can help you feel more in control.</li>
                        </ol>

                        <p>Side effects of Cancer treatment:</p>
                        <ul>
                            <li>Anemia </li>
                            <li>Appetite Loss </li>
                            <li>Bleeding and Bruising (Thrombocytopenia) </li>
                            <li>Constipation</li>
                            <li>Delirium </li>
                            <li>Diarrhea </li>
                            <li>Edema </li>
                            <li>Fatigue </li>
                            <li>Hair Loss (Alopecia) </li>
                            <li>Infection and Neutropenia</li>
                            <li>Lymphedema </li>
                            <li>Memory or Concentration Problems </li>
                            <li>Mouth and Throat Problems </li>
                            <li>Nausea and Vomiting</li>
                            <li>Nerve Problems (Peripheral Neuropathy) Pain </li>
                            <li>Sexual and Fertility Problems (Men & Women) </li>
                            <li>Skin and Nail Changes </li>
                            <li>Sleep Problems </li>
                            <li>Urinary and Bladder Problems</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr class="m-0" />
@include('includes.spread-the-word')
<hr class="m-0" />
@include('includes.our-partners', ['extraClass' => 'partners-bg bg-gray-light text-'])
<hr class="m-0" />


@endsection