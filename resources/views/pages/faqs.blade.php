@extends('layouts.app') @section('content')
<div class="hero" data-trigger="slick">

    <div class="item" style="background-image: url('{{ asset('images/faqs/banner.jpg') }}'); background-position: center center;">
        <div class="overlay"></div>

        <div class="container h-100">
            <div class="row h-100">
                <div class="col-md-6 col-xs-12 my-auto">
                    <div class="caption">
                        <h1 class="text-uppercase title">Frequently Asked Questions</h1>
                        <p>Here are all the queries about Cancer answered for you.</p>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

@include('includes.navbar-primary')

<div class="section pb-5 pt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-12 pr-5 pl-5 pb-5 tabs">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link border active" id="v-pills-tab-1" data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-home"
                        aria-selected="true">Cancer Related <i class="fa fa-angle-right pull-right"></i></a>
                    <a class="nav-link border border-top-0" id="v-pills-tab-2" data-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-profile" aria-selected="false">Causes of Cancer <i class="fa fa-angle-right pull-right"></i></a>
                    <a class="nav-link border border-top-0" id="v-pills-tab-3" data-toggle="pill" href="#v-pills-3" role="tab" aria-controls="v-pills-messages" aria-selected="false">Cancer Screenings <i class="fa fa-angle-right pull-right"></i></a>
                    <a class="nav-link border border-top-0" id="v-pills-tab-4" data-toggle="pill" href="#v-pills-4" role="tab" aria-controls="v-pills-messages" aria-selected="false">Cancer and Other Diseases <i class="fa fa-angle-right pull-right"></i></a>
                </div>
            </div>
            <div class="col-md-8 col-xs-12">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-tab-1">
                        <div id="accordion" role="tablist">
                            <div class="card">
                                <div class="card-header" data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" role="tab" id="headingOne">
                                    <h5 class="mb-0 d-inline text-primary">
                                            How does cancer spread?
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        When cancer cells break away from a tumor, they can travel to other parts of the body through the bloodstream or the lymph
                                        system (Lymph vessels are much like blood vessels, except they carry a clear fluid
                                        and immune system cells).
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header collapsed" role="tab" id="headingTwo" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <h5 class="mb-0 d-inline text-primary">

                                            Are cancers in men and women different?

                                    </h5>
                                </div>
                                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>No. However, men are more susceptible to lung, testes, prostate and oral cancers
                                            and women to breast and cervix cancers.</p>
                                        <p>Cancer of the mouth, throat, gullet, stomach, rectum, larynx, lung, skin and brain
                                            are all more common in men than in women. Gallbladder and thyroid cancers are
                                            more frequent in women.</p>
                                        <p>Common cancers in men are:</p>
                                        <ul>
                                            <li> Lip/oral cavity</li>
                                            <li>Lung</li>
                                            <li>Colorectum</li>
                                            <li>Prostate </li>
                                            <li>Pharynx</li>
                                        </ul>
                                        <p>While among women most common cancers are: </p>
                                        <ul>
                                            <li>Breast</li>
                                            <li>Cervix</li>
                                            <li>Colorectum</li>
                                            <li>Ovary</li>
                                            <li>Lip/oral cavity</li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header collapsed" role="tab" id="headingThree" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <h5 class="mb-0 d-inline text-primary">

                                            Is cancer hereditary?

                                    </h5>
                                </div>
                                <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Largely, no. There are probably some inherited tendencies that may lead to cancers
                                            of different types. The presence of cancer in one or both parents and/or close
                                            relatives should be a cause for greater alertness in looking for and recognising
                                            suspicious symptoms. However, one type of eye cancer known as Retinoblastoma
                                            can be inherited. </p>
                                        <p>Some cancers are genetically linked and may run in families but not all the cancers
                                            are hereditary. Cancer is a common disease and many families have at least a
                                            few members who have had cancer. This is because family members have certain
                                            risk factors in common, such as smoking, which can cause many types of cancer.
                                            It can also be due to other factors, like obesity, that tend to run in families
                                            and influence cancer risk. But most cancers are not clearly linked to the genes
                                            we inherit from our parents. Some features of genetically linked cancers are:
                                            </p>
                                        <ul>
                                            <li>Cancers occurring at younger ages than usual (like colon cancer in a 20 year
                                                old)</li>
                                            <li>More than one type of cancer in a single person (like a woman with both breast
                                                and ovarian cancer)</li>
                                            <li>Cancers occurring in both of a pair of organs (both eyes, both kidneys, both
                                                breasts)</li>
                                            <li>More than one childhood cancer in a set of siblings (like sarcoma in both a brother
                                                and a sister)</li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header collapsed" role="tab" id="headingFour" data-toggle="collapse" href="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                                    <h5 class="mb-0 d-inline text-primary">

                                            Is cancer contagious or infectious?

                                    </h5>
                                </div>
                                <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour" data-parent="#accordion">
                                    <div class="card-body">
                                        Cancer is not a contagious or infectious disease that easily spreads from person to person. It cannot spread through close
                                        contact like touching, sharing meals or breathing the same air

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header collapsed" role="tab" id="headingFive" data-toggle="collapse" href="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                                    <h5 class="mb-0 d-inline text-primary">

                                            Can you lead a normal life after cancer?

                                    </h5>
                                </div>
                                <div id="collapseFive" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>A person can lead a normal life after cancer but one has to go for regular checkups
                                            and follow up. But beyond initial recovery, there are ways to improve long-term
                                            health so that one can enjoy the years to come.</p>
                                        <p>The recommendations for cancer survivors are similar to healthy individuals i.e.
                                            balanced diet, exercise, healthy weight, reduce stress, good sleep, avoid tobacco
                                            and limit the amount of alcohol.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header collapsed" role="tab" id="headingSix" data-toggle="collapse" href="#collapseSix" aria-expanded="false" aria-controls="collapseThree">
                                    <h5 class="mb-0 d-inline text-primary">

                                            Does cancer develop more quickly in children than in older people?

                                    </h5>
                                </div>
                                <div id="collapseSix" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        Cancer does not develop more quickly in children and is much more common in older people. The commonest cancers in children
                                        in India are leukemia and lymphoma.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-tab-2">


                        <div id="accordion2" role="tablist">
                            <div class="card">
                                <div class="card-header" role="tab" data-toggle="collapse" href="#collapseOne1" aria-expanded1="true" aria-controls="collapseOne">
                                    <h5 class="mb-0 d-inline text-primary">

                                            Is there any relation between food and cancer?
                                    </h5>
                                </div>

                                <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion2">
                                    <div class="card-body">
                                        <p>Eating a healthy balanced diet is important for keeping a healthy body weight as
                                            obesity increases the risk of cancer.</p>
                                        <p>Some foods can also affect cancer risk:</p>
                                        <ul>
                                            <li>Foods like fruits, vegetables and foods high in fibre can reduce the risk of
                                                cancer</li>
                                            <li>Salt preserved foods can increase the risk of developing stomach cancer</li>
                                            <li>A diet high in processed and red meat can increase the risk of bowel cancer</li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header collapsed" role="tab" id="headingTwo" data-toggle="collapse" href="#collapseTwo1" aria-expanded="false" aria-controls="collapseTwo">
                                    <h5 class="mb-0 d-inline text-primary">

                                            Does Alcohol consumption lead to cancer?
                                    </h5>
                                </div>
                                <div id="collapseTwo1" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion2">
                                    <div class="card-body">
                                        Alcohol is a predisposing factor to the development of cancer of the oesophagus, laryngopharynx and liver.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header collapsed" role="tab" id="headingThree" data-toggle="collapse" href="#collapseThree1" aria-expanded="false" aria-controls="collapseThree">
                                    <h5 class="mb-0 d-inline text-primary">

                                            Is cancer caused by germs?
                                    </h5>
                                </div>
                                <div id="collapseThree1" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion2">
                                    <div class="card-body">
                                        There is no scientific evidence that cancer is directly caused by a germ. Although certain viruses are known to cause cancer,
                                        they form less than 2% of the cancer burden.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header collapsed" role="tab" id="headingFour" data-toggle="collapse" href="#collapseFour1" aria-expanded="false" aria-controls="collapseThree1">
                                    <h5 class="mb-0 d-inline text-primary">

                                            Can cancer result from sexual intercourse?
                                    </h5>
                                </div>
                                <div id="collapseFour1" class="collapse" role="tabpanel" aria-labelledby="headingThree1" data-parent="#accordion2">
                                    <div class="card-body">
                                        Cancer cannot result from sexual intercourse but Human PapillomaVirus (HPV) which increases the risk of certain cancers is
                                        transmitted through sexual contact. However not everyone who has HPV will develop
                                        cancer because of it. Other factors are also involved.

                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="tab-pane fade" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-tab-3">



                        <div id="accordion3" role="tablist">
                            <div class="card">
                                <div class="card-header" role="tab" id="headingOne" data-toggle="collapse" href="#collapseOne3" aria-expanded="true" aria-controls="collapseOne">
                                    <h5 class="mb-0 d-inline text-primary">

                                            What are the do’s/don’ts prior to cancer screening?
                                    </h5>
                                </div>

                                <div id="collapseOne3" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion3">
                                 <div class="card-body">
                                        <p>It is best to schedule a mammogram a week after the menstrual period as the breasts
                                            are least likely to be tender during this period. Also, it is best to avoid using
                                            deodorants, antiperspirants, powder etc on the day of the mammogram as metallic
                                            particles in deodorants and powders can show up on mammograms and at times cause
                                            confusion.</p>
                                        <p>Preparation prior to a Pap smear test includes the avoidance of sexual intercourse,
                                            douching or using vaginal medicines for two days prior to the test in order to
                                            avoid the abnormal cells from being washed away.</p>
                                        <p>Prior to undertaking the faecal occult blood test, it is best to speak to your doctor
                                            as vitamin tablets and anti-inflammatory drugs may need to be withheld for two
                                            to three days.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header collapsed" role="tab" id="headingTw3o" data-toggle="collapse" href="#collapseTwo3" aria-expanded="false" aria-controls="collapseTwo">
                                    <h5 class="mb-0 d-inline text-primary">

                                            At what age must one begin cancer screening?
                                    </h5>
                                </div>
                                <div id="collapseTwo3" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion3">
                                    <div class="card-body">
                                        <p>Women:</p>
                                        <ul>
                                            <li>Cervical Cancer: 21 yrs and above</li>
                                            <li>Breast Cancer: 40 yrs and above</li>
                                        </ul>
                                        <p>Men:</p>
                                        <ul>
                                            <li>Lung Cancer: 50 yrs and above</li>
                                            <li>Rectal and Colon Cancer: 50 yrs and above</li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header collapsed" role="tab" id="headingThree3" data-toggle="collapse" href="#collapseThree3" aria-expanded="false" aria-controls="collapseThree">
                                    <h5 class="mb-0 d-inline text-primary">

                                            Is there tax benefit for cancer screening?
                                    </h5>
                                </div>
                                <div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion3">
                                    <div class="card-body">
                                        For health check-ups, within the maximum limit of Rs. 25,000 or Rs. 30,000, the preventive health check-ups get a benefit
                                        of up to Rs. 5,000. This means, if you pay premium of Rs. 20,000 towards Mediclaim
                                        and undergo a health check-up costing Rs. 5,000, the total of Rs. 25,000 can be availed
                                        under section 80D.
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                    <div class="tab-pane fade" id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-tab-4">

                        <div id="accordion4" role="tablist">
                            <div class="card">
                                <div class="card-header" role="tab" id="headingOne" data-toggle="collapse" href="#collapseOne4" aria-expanded="true" aria-controls="collapseOne">
                                    <h5 class="mb-0 d-inline text-primary">

                                            Do piles turn into cancer?
                                    </h5>
                                </div>

                                <div id="collapseOne4" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion4">
                                    <div class="card-body">
                                        Piles do not turn into cancer. Piles or haemorrhoids are merely enlarged veins in the rectal wall. Cancer is occasionally
                                        found in the tissue above the haemorrhoids. Cancer is occasionally found in the tissue
                                        above the haemorrhoids. Bleeding piles should be examined carefully to determine
                                        whether cancer is present. It is good to go to a doctor if one has bleeding per anus
                                        to rule out cancer which also presents with bleeding and other symptoms.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header collapsed" role="tab" id="headingTwo" data-toggle="collapse" href="#collapseTwo4" aria-expanded="false" aria-controls="collapseTwo">
                                    <h5 class="mb-0 d-inline text-primary">

                                            Can a tuberculous person ever have cancer?
                                    </h5>
                                </div>
                                <div id="collapseTwo4" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion4">
                                    <div class="card-body">
                                        <p>Lung cancer can be frequently misdiagnosed as tuberculosis, especially in India.
                                            </p>
                                        <p>There are many similarities between both diseases like they both are very common,
                                            have high prevalence, involve lung parenchyma and above all, characterised by
                                            similar symptoms. But, there are many differences between these two entities
                                            like they have different etiologies (pulmonary tuberculosis is infectious while
                                            lung cancer is non-infectious disease), different consequences, and altogether
                                            different management. Delay in the diagnosis and treatment of lung cancer results
                                            in poorer outcome and lower survival.</p>
                                        <p>Person with TB can have cancer like any other person without TB.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header collapsed" role="tab" id="headingThree" data-toggle="collapse" href="#collapseThree4" aria-expanded="false" aria-controls="collapseThree">
                                    <h5 class="mb-0 d-inline text-primary">

                                            Are AIDS and cancer related?
                                    </h5>
                                </div>
                                <div id="collapseThree4" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion4">
                                    <div class="card-body">
                                        People with AIDS have an increased risk of developing certain cancers as kaposi’s sarcoma, non Hodgkin’s lymphoma and cervical
                                        cancer. The exact reason is not known but most likely the weakened immune system.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header collapsed" role="tab" id="headingThree" data-toggle="collapse" href="#collapseFour4" aria-expanded="false" aria-controls="collapseThree">
                                    <h5 class="mb-0 d-inline text-primary">

                                            Are Cancer and leprosy related?
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseFour4" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion4">
                                    <div class="card-body">
                                        Leprosy is an infectious disease, often treatable with antibiotics. Cancer is a different disease altogether and the two
                                        are not related.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('includes.ask-expert', ['type' => 'two'])
<hr class="m-0" /> @include('includes.spread-the-word')
<hr class="m-0" /> @include('includes.our-partners', ['extraClass' => 'bg-gray-light text-light partners-bg'])
<hr class="m-0" /> @endsection