@extends('layouts.app') @section('content')
<div class="hero" data-trigger="slick">

    <div class="item" style="background-image: url('{{ asset('images/partners/banner.jpg') }}'); background-position: center center;">
        <div class="overlay"></div>

        <div class="container h-100">
            <div class="row h-100">
                <div class="col-md-6 col-xs-12 my-auto">
                    <div class="caption">
                        <h1 class="text-uppercase title">OUR PARTNERS</h1>
                        <p>Partners who make our work achievable</p>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

@include('includes.navbar-primary')

<div class="section bg-white pt-5 pb-5">
    
    <div class="container text-center">
        <div class="row">
            <div class="col-md-12">
                
                <ul class="nav nav-pills nav-justified" id="myTab" role="tablist">
                    <li class="nav-item active">
                        <a class="nav-link active" id="tab1" data-toggle="tab" href="#tab-1" role="tab" aria-controls="tab-1" aria-selected="true">
                            <img class="img-fluid mb-3" src="{{ asset('images/apollo-square.jpg') }}" />
                            <h5 class="text-uppercase text-left">APOLLO CANCER Care Centre</h5>
                            
                            <div class="row">
                                <div class="col">
                                    <i class="fa fa-angle-down pull-left fa-2x"></i>
                                </div>
                                <div class="col">
                                    <i class="fa fa-angle-up pull-right fa-2x"></i>
                                </div>
                            </div>
                            
                            <!-- <p class="small text-left">It's time to get to know cancer.</p> -->
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab2" data-toggle="tab" href="#tab-2" role="tab" aria-controls="tab-2" aria-selected="false">
                            <img class="img-fluid mb-3" src="{{ asset('images/ics.jpg') }}" />
                            <h5 class="text-uppercase text-left">Indian Cancer Society</h5>
                            
                            <div class="row">
                                <div class="col">
                                    <i class="fa fa-angle-down pull-left fa-2x"></i>
                                </div>
                                <div class="col">
                                    <i class="fa fa-angle-up pull-right fa-2x"></i>
                                </div>
                            </div>
                            
                            <!-- <p class="small text-left">The different types of Cancers and their categorisation.</p> -->
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab3" data-toggle="tab" href="#tab-3" role="tab" aria-controls="tab-3" aria-selected="false">
                            <img class="img-fluid mb-3" src="{{ asset('images/life-again.jpg') }}" />
                            <h5 class="text-uppercase text-left">LIfe Again FOundation</h5>
                            
                            <div class="row">
                                <div class="col">
                                    <i class="fa fa-angle-down pull-left fa-2x"></i>
                                </div>
                                <div class="col">
                                    <i class="fa fa-angle-up pull-right fa-2x"></i>
                                </div>
                            </div>
                            
                            <!-- <p class="small text-left">Know the different stages of Cancer based on its extent and severity.</p> -->
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab4" data-toggle="tab" href="#tab-4" role="tab" aria-controls="tab-4" aria-selected="false">
                            <img class="img-fluid mb-3" src="{{ asset('images/can-support.jpg') }}" />
                            <h5 class="text-uppercase text-left">Cansupport</h5>
                            
                            <div class="row">
                                <div class="col">
                                    <i class="fa fa-angle-down pull-left fa-2x"></i>
                                </div>
                                <div class="col">
                                    <i class="fa fa-angle-up pull-right fa-2x"></i>
                                </div>
                            </div>
                            
                            <!-- <p class="small text-left">There are various types of cancer treatment. Know about them.</p> -->
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tab5" data-toggle="tab" href="#tab-5" role="tab" aria-controls="tab-5" aria-selected="false">
                            <img class="img-fluid mb-3" src="{{ asset('images/naturals.jpg') }}" />
                            <h5 class="text-uppercase text-left">Naturals</h5>
                            
                            <div class="row">
                                <div class="col">
                                    <i class="fa fa-angle-down pull-left fa-2x"></i>
                                </div>
                                <div class="col">
                                    <i class="fa fa-angle-up pull-right fa-2x"></i>
                                </div>
                            </div>
                            
                            <!-- <p class="small text-left">There are various types of cancer treatment. Know about them.</p> -->
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active text-left pt-3" id="tab-1" role="tabpanel" aria-labelledby="tab-1">
                    <p>A decade after the establishment of India’s first corporate hospital, Apollo Hospitals in Chennai, on 30th September 1993, Dr. Reddy launched a dedicated state-of-the-art facility for cancer care in the same city.</p>
                    <p>Originally named Apollo Cancer Hospital, it commenced operations as a 45-bed unit with medical oncology. Eventually other treatment modalities like surgical and radiation oncology were added to the overall cancer offering.</p>
                    <p>Subsequently, the hospital was renamed the Apollo Specialty Hospital with the addition of another super specialty - Neurosurgery. The facility has grown, and is now a 300-bed multi-specialty hospital with over 100 oncology specialists.</p>
                    <p>Dr. Reddy’s mission has been to spread the message that ‘Cancer is Conquerable’. This ethos led to the establishment of 10 dedicated Apollo Cancer Hospitals across India – Chennai, Kolkata, Delhi, Bangalore, Hyderabad, Madurai, Ahmedabad, Bilaspur, Bhubaneshwar & Mumbai. (Apollo Cancer Institute in Chennai is one of the most experienced cancer hospitals in the world and it is going to complete 25 years of service on 30th Sep’18).</p>
                    <p>The Apollo Hospitals Cancer Group have over 150 surgical and radiation cancer specialists as well as diagnostic consultants. The centre's doctors examine every case jointly and decide on the best line of treatment for the patient. As the treatment of cancer involves tremendous physical and emotional strain, the hospital provides extra support to cancer patients by counselling them to stay positive and to eat right - thereby speeding up the recovery process. Specially trained medical counsellors, speech therapists, dieticians and other professionals, provide support to the teams. The measurable success that the hospitals have achieved in cancer care attract thousands of national and international patients each year.</p>
                    <p>The Apollo Hospitals Cancer Group has consistently helped cancer care in India to break new ground. The Group has nurtured an ecosystem of excellence that has pioneered new treatment procedures and embraced cutting-edge technology with the introduction of futuristic equipment like the CyberKnife, Novalis TX and True Beam in India.</p>
                    <p>By September 2018, Apollo Hospitals will be establishing the ﬁrst Proton Beam Therapy Centre for cancer treatment to serve over 3.5 billion people. This Proton Beam Therapy Centre will be the ﬁrst-of-its-kind across South-East Asia, Africa and Australia.</p>

                    </div>
                    <div class="tab-pane fade text-left pt-3" id="tab-2" aria-labelledby="tab-2">
                    <p>Indian Cancer Society was established in 1951 by Dr. D.J. Jussawalla and Mr. Naval Tata as India's first voluntary, non-profit, National Organization for Awareness, Detection, Cure and Survivorship of those affected with this disease.</p>
<p>The Society’s activities encompass the entire continuum of cancer care – cancer awareness, screening for early detection, financial help for treatment, support groups, rehabilitation of cancer survivors, registry, research and education.</p>
<p>Cancer Awareness:  The Society creates awareness about cancer through various programs such as: Awareness talks conducted in schools, colleges, corporates and the community; informative videos and posts on social media; educational films for breast, cervical, oral cancers and dangers of smoking and chewing tobacco.</p>
<p>Screening for early detection: Many cancers are curable when detected early and treated adequately.  The society has facilitated screening for more than three lakh people through 2500 camps across Mumbai, Maharashtra and other states. These tests are done free of cost for the underprivileged. The suspect cases identified are further assisted in accessing diagnostic tests and treatment at the nearest cancer hospitals.</p>
<p>Cancer Cure Fund: provides financial aid for treatment of underprivileged Cancer patients. </p>
                    </div>
                    <div class="tab-pane fade text-left pt-3" id="tab-3" aria-labelledby="tab-3">
                    <p>Life Again, a worldwide not-for-profit organization that gives hope and a future for several of those whose journey of life has been full of challenges. Life Again is aimed to be a fuelling station of inspiration and encouragement in order to connect everyone through love towards humanity.</p>
<p>After three years of dedication, commitment, research and approaching several organizations, LIFE AGAIN was born as a solution. Life Again would like to serve, reach out and make changes that exist due to the lack of touch with humanity. Life Again Foundation is destined to give a message that when your body is itself a temple and you are real God. We simply say then, why don’t you play your role and become one? Why search everywhere?</p>

                    </div>
                    <div class="tab-pane fade text-left pt-3" id="tab-4" aria-labelledby="tab-4">
                    <p>CanSupport is a registered society established in 1996. It grew out of the personal experience of Ms. Harmala Gupta, a survivor of Hodgkin’s lymphoma.</p>
<p>CanSupport offers information, support and meaningful care to people with cancer and their families and has a spectrum of services to care for people who are at different stages of their journey with cancer.  All their services are free.</p>
<p>Cansupport’s Programs</p>
<p>(a) Home Based Palliative Care Services: Pioneering Home Based Palliative Care in Northern India with 6 patients in 1997, CanSupport is now the largest stand-alone Home Based Palliative Care organization in the country, providing care to about 2000 concurrent patients in Delhi and the National Capital Region through 28 multidisciplinary teams of highly trained & committed professionals.</p>
<p>(b) Training, Education & Research:Palliative Care is a developing Specialty in India which needs to be highlighted to the general public who don’t know about the benefits of it. Therefore, only a systematic training and education of medical staff will ensure expansion of Palliative Care in the whole of India.</p>
<p>(c) Outpatient Clinic: CanSupport runs a daily Pain & Palliative Care Outpatient Clinic in East Delhi. It is aimed at pain & symptom management, comprehensive nursing care, psychosocial support, among others.</p>
<p>(d) Peer Support: One-on-one support over the telephone or in person is provided by cancer survivors to those undergoing active treatment for cancer and their caregivers</p>
<p>(f) CanSupport Helpline: The Helpline (011-26711212) runs from Monday to Friday (9:30 am to 5:30 pm). It provides information and emotional support to cancer patients and their family members.</p>

                    </div>
                    <div class="tab-pane fade text-left pt-3" id="tab-5" aria-labelledby="tab-4">
                    <h5>ABOUT NATURALS:</h5>
<p>From 1 salon to over 600 salons across India, all it took was one woman’s passion. This drove Naturals to become India’s No.1 hair and beauty salon.</p>
<p>At Naturals, we believe in financial independence for women and have empowered 400+ women to become entrepreneurs in the past 16 years. Our dream is to create a housewife-free India, where women are encouraged to earn their living by pursuing their passion. By 2020, Naturals aims to create 3000 salons, empower 1000 women entrepreneurs and create 50,000 jobs, because there is no better style statement than standing on your own feet.</p>
<p>The first Naturals salon was opened in Khader Nawaz Khan Road, Chennai in 2000 with an investment of Rs. 30 lakh. Naturals adopted the franchise model business after setting up their 54th salon. Besides the unisex salons, the brand has Naturals lounge (premium salon), Naturals W (women salon), Naturals Ayur (Salon and wellness centre), Page 3 (luxury salons) and the recently launched Naturals Enable, making it the first inclusive salon chain in India. The vision of the company is to promote women entrepreneurs. Majority of the Naturals franchisee owners are women. The company has been successful in empowering about 600 women and provided jobs for about 15000. Naturals has training centers in Chennai and Madurai. The training academy provides hands on training at the beauty salons and ensures 100% placement at Naturals. There are currently 570 Naturals salons in India. The couple is aiming to raise the number to 3000 salons, 1000 women entrepreneurs and 50,000 jobs by 2020.</p>
<p>The salon chain also understands the beauty industry and has taken it upon themselves to create skilled talent. Naturals Training academy is a state-of-the-art facility on mission to transform the hair and beauty industry in India, The primary objective of the academy is to educate aspiring individuals in the art of hair and beauty and provide job opportunities. The academy has a track record of 100% placement at Naturals.</p>
<p></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr class="m-0" />
@include('includes.spread-the-word')
<hr class="m-0" />
@include('includes.our-partners', ['extraClass' => 'partners-bg bg-gray-light text-'])
<hr class="m-0" />


@endsection