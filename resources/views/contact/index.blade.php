@extends('layouts.app') @section('content')
<div class="hero" data-trigger="slick">

    <div class="item" style="background: url('{{ asset('images/contact-us/banner.jpg') }}'); background-position: center center;">
        <div class="overlay"></div>

        <div class="container h-100">
            <div class="row h-100">
                <div class="col-md-6 col-xs-12 my-auto">
                    <div class="caption">
                        <h1 class="text-uppercase title">Contact Us</h1>
                        <p> </p>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

@include('includes.navbar-primary')
<div class="section bg-secondary text-light">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 offset-md-2 p-5">
                <h2>GET IN TOUCH WITH US</h2>
                @if (session('status') && session('status') == true)
                <div class="alert alert-success">
                    Thank you for contacting. You query has been forwarded.
                </div>
                @elseif(session('status') && session('status') == false)
                <div class="alert alert-danger">
                    An error occured. Please try again later
                </div>
                @endif

                <form action="/contact" method="post" id="contac-form">
                    <div class="form-row">
                        <div class="form-group col-md-3 {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="name">Name *</label>
                            <input type="name" class="form-control" id="name" name="name" placeholder="Name" value="{{ old('name') }}">
                            <span class="text-light small">{{ $errors->first('name') }}</span>
                        </div>
                        <div class="form-group col-md-3 {{ $errors->has('email') ? 'has-error' : '' }}">
                            <label for="email">Email *</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="{{ old('email') }}">
                            <span class="text-light small">{{ $errors->first('email') }}</span>
                        </div>

                        <div class="form-group col-md-3 {{ $errors->has('email') ? 'has-error' : '' }}">
                            <label for="type">Query Type *</label>
                            <select class="form-control" name='type' id="type">
                                <option value="campaign">Campaign Related Queries</option>
                                <option value="cancer">Cancer Related Queries</option>
                            </select>
                            <span class="text-light small">{{ $errors->first('type') }}</span>
                        </div>
                        <div class="form-group col-md-3 {{ $errors->has('phone') ? 'has-error' : '' }}">
                            <label for="phone">Phone</label>
                            <input type="phone" maxlength="10" name="phone" class="form-control" id="phone" placeholder="Phone" value="{{ old('phone') }}">
                            <span class="text-light small">{{ $errors->first('phone') }}</span>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('message') ? 'has-error' : '' }}">
                        <label for="message">Your Query</label>
                        <textarea class="form-control" id="mesasge" name="message" placeholder="Type your message here">{{ old('message') }}</textarea>
                        <span class="text-light small">{{ $errors->first('message') }}</span>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-light">SUBMIT</button>
                </form>
                <p class="mt-3"> For any queries regarding Outsmart Cancer, please get in touch with us at
                    <u><em><a class="text-light" href="mailto:info@outsmartcancer.in">info@outsmartcancer.in</a></em></u>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="container mt-5 pb-5" id="partners">
    <div class="row">
        <div class="col-md-12 text-center">
            <p>Feel free to get in touch with our partners on their helpline numbers for assistance on any query you have or

            <a href="#" onclick="return false" data-toggle="tooltip" data-placement="top" title="Coming soon">
                    <u>ask our expert.</u>
                </a>
            </p>
        </div>
        <div class="col-md-12">
            <div class="card-deck">
                <div class="card text-center">
                    <a target="_blank" href="{{ url('https://www.apollohospitals.com/departments/cancer ') }}"><img class="card-img-top" src="{{ asset('images/apollo-square.jpg') }}" alt=""></a>
                    <div class="card-body">
                        <p class="card-title">
                            <strong>Contact Number: 18605001066</strong>
                        </p>

                    </div>
                    <!-- <div class="card-footer bg-light">
                        <a href="{{ url('https://www.apollohospitals.com/departments/cancer ') }}" class="btn btn-outline-primary">Website</a>
                    </div> -->
                </div>

                <div class="card text-center">
                    <a target="_blank" href="{{ url('http://www.indiancancersociety.org') }}" ><img class="card-img-top" src="{{ asset('images/ics.jpg') }}" alt=""></a>
                    <div class="card-body">
                        <p class="card-title">
                            <strong>Contact Number: 18001951</strong>
                        </p>
                    </div>
                    <!-- <div class="card-footer bg-light">
                        <a href="{{ url('http://www.indiancancersociety.org') }}" class="btn btn-outline-primary">Website</a>
                    </div> -->
                </div>

                <div class="card text-center">
                    <a target="_blank" href="{{ url('http://www.cansupport.org/') }}" ><img class="card-img-top" src="{{ asset('images/can-support.jpg') }}" alt=""></a>
                    <div class="card-body">
                        <p class="card-title">
                            <strong>Contact Number: 011-26711212 </strong>(Runs from Monday to Friday from 9:30 am to 5:30 pm)
                        </p>

                    </div>
                    <!-- <div class="card-footer bg-light">
                        <a href="{{ url('http://www.cansupport.org/') }}" class="btn btn-outline-primary disabled" disabled="disabled">Website</a>
                    </div> -->
                </div>
                <div class="card text-center">
                    <a target="_blank" href="{{ url('https://www.facebook.com/LifeAgainIndia/') }}" ><img class="card-img-top" src="{{ asset('images/life-again.jpg') }}" alt=""></a>
                    <div class="card-body">
                        <p class="card-title">
                            <strong>Contact Number: 044 4958 5275</strong>
                        </p>

                    </div>
                    <!-- <div class="card-footer bg-light">
                        <a href="{{ url('https://www.facebook.com/LifeAgainIndia/') }}" class="btn btn-outline-primary disabled" disabled="disabled">Website</a>
                    </div> -->
                </div>
            </div>
        </div>

    </div>
</div>
<hr class="m-0" /> @include('includes.our-partners', ['extraClass' => 'bg-gray-light text-light partners-bg'])
<hr class="m-0" /> @endsection