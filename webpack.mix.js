let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js([
        'resources/assets/js/bootstrap.js',
        'node_modules/popper.js/dist/popper.js',
        'node_modules/jquery/dist/jquery.js',
        'node_modules/bootstrap/dist/js/bootstrap.js',
        'node_modules/slick-carousel/slick/slick.js',
        "node_modules/jssocials/dist/jssocials.js",
        'node_modules/odometer/odometer.js',
        'resources/assets/js/application.js',
    ], 
    'public/js/application.js')
   .sass('resources/assets/sass/application.scss', 'public/css/application.css');



if (mix.inProduction()) {
    mix.version();
}