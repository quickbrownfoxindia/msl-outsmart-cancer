<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('oauth/{provider}', 'OAuthController@redirectToProvider');
Route::get('oauth/{provider}/callback', 'OAuthController@handleProviderCallback');
Route::get('home', function() {
    return redirect('/#ask-an-expert-wrapper');
});


Auth::routes();
Route::group(['middleware' => 'web'], function() {
    
    Route::get('/', 'HomeController@index')->name('home');
    Route::post('pledges', 'PledgesController@store')->name('plege');
    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
    Route::get('contact', 'ContactController@index');
    
    Route::post('contact', 'ContactController@store');
    Route::get('ajax/cities/{query}', 'AjaxController@cities');
    Route::get('ajax/states/{query}', 'AjaxController@states');
    Route::get('all-about-cancer', 'PagesController@allAboutCancer');
    Route::get('cancer-screening', 'PagesController@cancerScreening');
    Route::get('faqs', 'PagesController@faqs');
    Route::get('get-involved', 'PagesController@getInvolved');
    Route::get('partners', 'PagesController@partners');
    Route::get('ajax/get-pledge-count', 'AjaxController@getPledgeCount');

    Route::get('form', 'HomeController@form');
    Route::get('call', 'HomeController@call');

});